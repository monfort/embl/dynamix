define(
    [
        "dojo/_base/declare",
        "dojo/_base/fx",
        "dojo/_base/lang",
        "dojo/_base/array",
        "dojo/_base/json",
        "dojo/dom-class",
        'dojo/aspect',
        "dojo/topic",
        'dojo/request/xhr',
        "dojo/Deferred",
        "dojo/promise/all",
        "dojo/when",
        "dojo/dom-style",
        "dojo/dom-construct",
        "dojo/dom-attr",
        "dojo/query",
        "dojo/Stateful",
        "dojo/dom",
        "dojo/mouse",
        "dojo/on",
        "dojo/cookie",
        'dijit/form/Select',
        "dijit/registry",
        "dijit/Destroyable",
        'JBrowse/Util',
        'JBrowse/View/Track/LocationScale',
        './widgets/dxTrackGroupWidget'
    ],
    function(
        declare,
        baseFx,
        lang,
        array,
        JSON,
        domClass,
        aspect,
        topic,
        xhr,
        Deferred,
        pall,
        when,
        domStyle,
        domConstruct,
        domAttr,
        query,
        Stateful,
        dom,
        mouse,
        on,
        cookie,
        Select,
        registry,
        Destroyable,
        Util,
        LocationScaleTrack,
        dxTrackGroupWidget
    ) {
        return declare([Stateful, Destroyable], {
            /* (*) attribute initialised by constructor */
            // No need for default value since the main gives default values
            // or cookies values
            dx: null, // (*)
            groupId: "", // (*)

            groupMode: "", // (*) auto / manu / disabled
            groupTrackName: "", // (*)
            groupRange: 0, // (*)
            groupDefaultAnchor: "",
            barLoop: false,

            groupStackOrder: 0,
            groupTrackList: "", // (*)
            groupTrackIdsList: [], //new
            groupTrackIdsListOrder: "",
            groupTrackLabelsList: null, //new
            groupDxidsToLabelMap: {},
            groupLabelsToDxidsMap: {},
            groupCount: 0,
            // Lists
            idsShownOnInterval: [],
            labelsShownOnInterval: [],
            groupTrackIdsToShow: [],

            significantMoveGenomicInterval: {},

            trackLabelsToShow: [],

            numberShownOnInterval: 0,
            numberRelevantOnInterval: 0,
            numberRelevantShownOnInterval: 0,

            groupMaxTracks: 0, // (*)
            isMaxTracksReached: false, //bool
            groupAllShown: false, //True if we see all tracks
            lastVisibleRegion: {},
            widget: null,

            groupColor:null,

            // viewChangeListener: null,
            currRefSeq: "",
            toggleFeatures: true,
            toggleSignal: true,
            parameterHash: {
                /* Hash linking configuration
                 ** file parameters to widgets parameters,
                 ** In the order they will be displayed
                 ** in the setting window */
                "group_track_name": "groupTrackName",
                "group_track_ids": "groupTrackLabelsList",
                "mode": "groupMode",
                "show_signal": "toggleSignal",
                "show_features": "toggleFeatures",
                "overlap_range": "groupRange",
                "max_tracks": "groupMaxTracks"
            },

            _barLoopGetter: function() {
                return this.barLoop;
            },
            _barLoopSetter: function(barloop) {
                this.barLoop = barloop;
            },
            _groupModeGetter: function() {
                return this.groupMode;
            },
            _groupModeSetter: function(md) {
                this.groupMode = md;
            },
            _groupDefaultAnchorGetter: function() {
                return this.groupDefaultAnchor;
            },
            _groupDefaultBeforeAnchorGetter: function() {
                return this.groupDefaultBeforeAnchor;
            },
            _groupRangeGetter: function() {
                return this.groupRange;
            },
            _groupRangeSetter: function(rg) {
                this.groupRange = rg;
            },
            _toggleFeaturesGetter: function() {
                return this.toggleFeatures;
            },
            _toggleFeaturesSetter: function(sf) {
                this.toggleFeatures = sf;
            },
            _toggleSignalGetter: function() {
                return this.toggleSignal;
            },
            _toggleSignalSetter: function(ss) {
                this.toggleSignal = ss;
            },
            _groupTrackListGetter: function() {
                return this.groupTrackList;
            },
            _idsShownOnIntervalGetter: function() {
                return this.idsShownOnInterval;
            },
            _idsShownOnIntervalSetter: function(shwd) {
                this.idsShownOnInterval = shwd;
            },
            _labelsShownOnIntervalGetter: function() {
                return this.labelsShownOnInterval;
            },
            _labelsShownOnIntervalSetter: function(shwd) {
                this.labelsShownOnInterval = shwd;
            },
            _numberShownOnIntervalGetter: function() {
                return this.numberShownOnInterval;
            },
            _numberShownOnIntervalSetter: function(numShwd) {
                this.numberShownOnInterval = numShwd;
            },
            _numberRelevantOnIntervalGetter: function() {
                return this.numberRelevantOnInterval;
            },
            _numberRelevantOnIntervalSetter: function(numRlvnt) {
                this.numberRelevantOnInterval = numRlvnt;
            },
            _groupTrackIdsToShowGetter: function() {
                return this.groupTrackIdsToShow;
            },
            _groupTrackIdsToShowSetter: function(grpTrckIdsToShow) {
                this.groupTrackIdsToShow = grpTrckIdsToShow;
            },
            _trackLabelsToShowGetter: function() {
                return this.trackLabelsToShow;
            },
            _trackLabelsToShowSetter: function(grpTrckLblsToShow) {
                this.trackLabelsToShow = grpTrckLblsToShow;
            },
            _trackLabelsToHideSetter: function(grpTrckLblsToHide) {
                this.trackLabelsToHide = grpTrckLblsToHide;
            },
            _trackLabelsToHideGetter: function() {
                return this.trackLabelsToHide;
            },
            _groupMaxTracksGetter: function() {
                return this.groupMaxTracks;
            },
            _groupMaxTracksSetter: function(grpMxTrcks) {
                this.groupMaxTracks = grpMxTrcks;
            },
            _isMaxTracksReachedGetter: function() {
                return this.isMaxTracksReached;
            },
            _isMaxTracksReachedSetter: function(mxTrckRchd) {
                this.isMaxTracksReached = mxTrckRchd;
            },
            _groupAllShownGetter: function() {
                return this.groupAllShown;
            },
            _groupAllShownSetter: function(allShwn) {
                this.groupAllShown = allShwn;
            },

            _relevantTrackIdsSetter: function(trackIds) {
                /*Put some stuff here ?*/
                this.relevantTrackIds = trackIds;
            },

            computeGroupDynamixIdsLabelsMaps: function(groupTrackConfigs) {
                var groupDxidsToLabelMap = {};
                var groupLabelsToDxidsMap = {};
                array.forEach(groupTrackConfigs, function(trackConf) {
                    dxId = trackConf["dynamix_id"] || "";
                    dxPtag = trackConf["dynamix_priority"] || "";
                    trackLabel = trackConf["label"];
                    groupDxidsToLabelMap[dxId] = groupDxidsToLabelMap[dxId] || {};
                    groupDxidsToLabelMap[dxId][trackLabel] = dxPtag;
                    groupLabelsToDxidsMap[trackLabel] = dxId;
                });
                this.groupDxidsToLabelMap = groupDxidsToLabelMap;
                this.groupLabelsToDxidsMap = groupLabelsToDxidsMap;
            },

            sortTrackListIds: function(listOrderedGroupTrackIdsList, configSortingKeyword) {
                // only if sorting aphabetically is asked, do something. else keep list order
                var sortedListIds = listOrderedGroupTrackIdsList;
                if (configSortingKeyword === 'alpha') {
                    sortedListIds.sort();
                }
                return sortedListIds;
            },

            checkGroupRange: function(groupRange) {
                if (!(this.dx.isNumber(groupRange))) groupRange = 0;
                return parseInt(groupRange, 10);
            },

            constructor: function(kwArgs) {
                lang.mixin(this, kwArgs);
                /*
                /* Lang.mixing calls allow to assign values from kwArgs, defined in main
                /* to this group attributes... Written here for clarity.
                /- this.dx = kwArgs["dx"];
                /- this.groupId = kwArgs["groupId"];
                /- this.groupConfiguration = kwArgs["groupConfiguration"];
                /- this.pluginAutoActivated = kwArgs["dxAutoActivation"];
                /- this.pluginCookieActivated = kwArgs["dxCookieActivation"];
                /- this.orderedGroupStackIds = kwArgs["orderedGroupStackIds"];
                /- this.groupTrackConfigs = kwArgs["groupTrackConfigs"];
                /- this.listOrderedGroupTrackIdsList = kwArgs["listOrderedGroupTrackIdsList"]
                */
                // init some needed
                this.visibleRegion = this.dx.browser.view.visibleRegion();
                this.currZoomLevel = this.dx.browser.view.curZoom;
                this.previousZoomLevel = this.currZoomLevel;
                this.groupTrackName = this.groupConfiguration['group_track_name'] || "Unknown";
                this.groupTrackStore = this.dx.getTrackStore(this.groupTrackName) || null;
                this.groupTrackIdsListOrder = this.groupConfiguration['order'] || 'alpha';
                var groupDefaultAnchor;
                var groupDefaultBeforeAnchor = false;
                if (typeof this.groupConfiguration['view_anchor'] !== "undefined"){
                    if (typeof this.groupConfiguration['view_anchor'] === 'string'){
                        groupDefaultAnchor = this.groupConfiguration['view_anchor'];
                    }
                    else if (Object.prototype.toString.call(this.groupConfiguration['view_anchor']) === '[object Array]'){
                        if (this.groupConfiguration['view_anchor'].length === 1) groupDefaultAnchor = this.groupConfiguration['view_anchor'][0];
                        else if (this.groupConfiguration['view_anchor'].length > 1){
                            groupDefaultAnchor = this.groupConfiguration['view_anchor'][1];
                            groupDefaultBeforeAnchor = this.groupConfiguration['view_anchor'][0] === "before" ? true : false;
                        }
                    }
                    this.groupDefaultBeforeAnchor = groupDefaultBeforeAnchor;
                    if (array.indexOf(Object.keys(this.dx.browser.trackConfigsByName), groupDefaultAnchor) !== -1) this.groupDefaultAnchor = groupDefaultAnchor;
                }

                this.groupStackOrder = array.indexOf(this.orderedGroupStackIds, this.groupId);
                // Sort ID list, according to sorting keyword in conf (alpha or list order).
                this.groupTrackIdsList = this.sortTrackListIds(this.listOrderedGroupTrackIdsList, this.groupTrackIdsListOrder);

                this.computeGroupDynamixIdsLabelsMaps(this.groupTrackConfigs);
                this.groupTrackLabelsList = Object.keys(this.groupLabelsToDxidsMap);
                this.groupCount = this.groupTrackLabelsList.length;

                this.enabled = false;
            },

            postscript: function() {
                this.inherited(arguments);
                /* WARN :
                 ** this.watch fire event only if the attribute is modified
                 ** with the this.set("ATTR", VAL) function
                 ** i.e. It does not work with this.ATTR = VAL */
                // This is called from MAIN
                var groupRange = this.dxGroupCookie("groupRange") || this.groupConfiguration['overlap_range'] || "0";
                this.groupRange = this.checkGroupRange(groupRange);
                this.groupMaxTracks = this.dxGroupCookie("groupMaxTracks") || this.groupConfiguration['max_tracks'] || "25";

                var cookieShowFeat;
                var cookieShowSign;
                if (this.dxGroupCookie("toggleFeatures") === true || this.dxGroupCookie("toggleFeatures") === "true") this.toggleFeatures = true;
                else if (this.dxGroupCookie("toggleFeatures") === false || this.dxGroupCookie("toggleFeatures") === "false") this.toggleFeatures = false;
                else if (this.groupConfiguration['show_features'] === true || this.groupConfiguration['show_features'] === "true" || typeof this.groupConfiguration['show_features'] === 'undefined') this.toggleFeatures= true;
                else if (this.groupConfiguration['show_features'] === false || this.groupConfiguration['show_features'] === "false") this.toggleFeatures= false;
                else this.toggleFeatures = true;

                if (this.dxGroupCookie("toggleSignal") === true || this.dxGroupCookie("toggleSignal") === "true") this.toggleSignal = true;
                else if (this.dxGroupCookie("toggleSignal") === false || this.dxGroupCookie("toggleSignal") === "false") this.toggleSignal = false;
                else if (this.groupConfiguration['show_signal'] === true || this.groupConfiguration['show_signal'] === "true" || typeof this.groupConfiguration['show_signal'] === 'undefined') this.toggleSignal= true;
                else if (this.groupConfiguration['show_signal'] === false || this.groupConfiguration['show_signal'] === "false") this.toggleSignal= false;
                else this.toggleSignal = true;
                this.groupMode = this.dxGroupCookie("groupMode") || this.groupConfiguration['mode'] || "disabled";
                // Handle some values that could be written by user in conf,
                if (this.groupMode === 'enabled') this.groupMode = 'manual';
                else if (this.groupMode === 'auto') this.groupMode = 'automatic';
                else if ((this.groupMode !== 'automatic')
                            && (this.groupMode !== 'manual')
                            && (this.groupMode !== 'disabled')) this.groupMode = 'disabled';
                this.widget = this.createWidget(this);
                this.watchGroupMode = this.enableGroupModeWatcher();
                this.own(
                    this.watchGroupMode
                );
                /* Create all the watchers */
                if (this.groupMode === 'automatic' || this.groupMode === 'manual'){
                    this.enableGroup();
                }

            },

            createWidget: function(thisGroup) {
                /* Create the widget associated to the group
                 ** Give him something to watch through  */

                var groupWidget = new dxTrackGroupWidget({
                    "parentGroup": thisGroup,
                    "gid": thisGroup.groupId,
                    "wid": "widget_" + thisGroup.groupId,
                    "dxName": thisGroup.dx.name,
                    "numberShownOnInterval": thisGroup.numberShownOnInterval,
                    "numberRelevantOnInterval": thisGroup.numberRelevantOnInterval,
                    "numberRelevantShownOnInterval": thisGroup.numberRelevantShownOnInterval,
                    "groupAllShown": thisGroup.groupAllShown,
                    "widgetGroupMode": thisGroup.groupMode,
                    "toggleSignal": thisGroup.toggleSignal,
                    "toggleFeatures": thisGroup.toggleFeatures,
                    "isMaxTracksReached": thisGroup.isMaxTracksReached,
                    "colorSq" : this.groupColor
                });
                thisGroup.own(
                    groupWidget,
                    on(groupWidget.selector, "change", lang.hitch(this, function(value){
                        thisGroup.set("groupMode", value);
                    }))
                );
                return groupWidget;
            },

            enableGroup: function() {
                if (this.enabled === false) {
                    /* Need cookies for the following */
                    this.watchGroupRange = this.enableGroupRangeWatcher();
                    this.watchGroupMaxTracks = this.enableGroupMaxTracksWatcher();
                    this.watchIsMaxTracksReached = this.enableIsMaxTracksReachedWatcher();
                    this.watchToggleFeatures = this.enableToggleFeaturesWatcher();
                    this.watchToggleSignal = this.enableToggleSignalWatcher();
                    /* No Need of cookies for the following */
                    this.watchShownLabels = this.enableShownLabelsWatcher();
                    // NB: watchShownLabels also updates relevantShownTrackLabels
                    this.watchShownLabelsCount = this.enableShownCountWatcher();
                    this.watchRelevantLabels = this.enableRelevantLabelsWatcher();
                    this.watchRelevantLabelsCount = this.enableRelevantCountWatcher();
                    this.watchShownRelevantLabels = this.enableRelevantShownLabelsWatcher();
                    this.watchShownRelevantLabelsCount = this.enableRelevantShownCountWatcher();
                    this.watchWidgetHidingTrack = this.enableDxWidgetHidTrackTopicListener();
                    this.watchUsrHidingTrack = this.enableDxUserHidTrackTopicListener();
                    this.hiddenTracksTopic = this.enableDxCoreHidTrackTopicListener();
                    this.myDxShowTopic = this.enableDxTrackShowTopic();
                    //
                    this.coloriseGroupTrackTopic = this.enableColoriseGroupTrackTopic();
                    //
                    this.own(
                        this.watchGroupRange,
                        this.watchToggleFeatures,
                        this.watchToggleSignal,
                        this.watchGroupMaxTracks,
                        this.watchIsMaxTracksReached,
                        //
                        this.watchShownLabels,
                        this.watchShownLabelsCount,
                        this.watchRelevantLabels,
                        this.watchRelevantLabelsCount,
                        this.watchShownRelevantLabels,
                        this.watchShownRelevantLabelsCount,
                        this.watchWidgetHidingTrack,
                        this.watchUsrHidingTrack,
                        this.hiddenTracksTopic,
                        this.myDxShowTopic,
                        //
                        this.coloriseGroupTrackTopic
                    );

                    this.dx.browser.publish('/dynamix/tracks/dx/colorise/'+this.groupId,
                                            this.getShownOnIntervalLabelsList(this.dx.browser.view.visibleTrackNames()));
                    this.enabled = !this.enabled; //toggle boolean
                }
            },


            /* Remove handlers and watchers*/
            disableGroup: function() {
                if (this.enabled === true) {
                    when(this.dx.controllerHideTracks(this.get("labelsShownOnInterval")), lang.hitch(this, function() {
                        this.removeHandlers();
                        this.enabled = !this.enabled;
                    }));
                }
            },

            removeHandlers: function() {
                // this.watchGroupMode.remove(); // NOPE, we keep this one! In case of re-activation
                this.watchGroupRange.remove();
                this.watchToggleFeatures.remove();
                this.watchToggleSignal.remove();
                this.watchGroupMaxTracks.remove(),
                this.watchIsMaxTracksReached.remove(),
                //
                this.watchShownLabels.remove();
                this.watchShownLabelsCount.remove();
                this.watchRelevantLabels.remove();
                this.watchRelevantLabelsCount.remove();
                this.watchShownRelevantLabels.remove();
                this.watchShownRelevantLabelsCount.remove();
                this.watchWidgetHidingTrack.remove();
                this.watchUsrHidingTrack.remove();
                this.hiddenTracksTopic.remove();
                this.myDxShowTopic.remove();
                //
                this.coloriseGroupTrackTopic.remove();
            },

            /**************************
            ------ HANDLERS ------
            **************************/
            /**************************
            ------ WATCHERS ------
            --------------------------------------------------------------------------------
            ** Watch certain of the class attributes to do
            ** real-time update of other dependant ones.
            ** This is use mainly by the widgets to update their content .
            --------------------------------------------------------------------------------
            **************************/


            enableGroupModeWatcher: function() {
                return this.watch("groupMode", lang.hitch(this, function(name, oldMode, newMode){
                    this.dxGroupCookie("groupMode", newMode);
                    if (this.widget.selector.get('value') !== newMode) this.widget.selector.set('value', newMode, false);
                    this.onGroupModeChange(name, oldMode, newMode);
                }));
            },

            /* THIS IS NOT A WATCHER BUT AN ASSOCIATED FUNCTION
            ** TRIGGERED BY THE GROUPMODE CHANGE WATCHER*/
            onGroupModeChange: function(name, oldMode, newMode) {
                if (oldMode === 'disabled' && (newMode === 'automatic' || newMode === 'manual')) this.enableGroup();
                if (newMode === 'automatic' || newMode === 'manual'){
                    // this.enableGroup();
                    /* Need to update information because the group was disabled
                     ** We simulate what happen after a move over the sensitivity threshold */
                    this.dx.afterMove( [this.groupId], this.dx.browser.view.visibleTrackNames());
                }
                if (oldMode !== 'disabled' && newMode === 'disabled') {
                    this.disableGroup();
                    this.widget.disableGroupWidget();
                }
            },

            enableGroupRangeWatcher: function() {
                return this.watch("groupRange", lang.hitch(this, function(name, oldRange, newRange){
                    this.dxGroupCookie("groupRange", newRange);
                }));
            },

            enableGroupMaxTracksWatcher: function() {
                return this.watch("groupMaxTracks", lang.hitch(this, function(name, oldMt, newMt){
                    this.dxGroupCookie("groupMaxTracks", newMt);
                    //if (this.get("shownTrackLabels") >= this.get("groupMaxTracks")) this.set("isMaxTracksReached", true);
                    //if (this.get("numberShownOnInterval") > this.get("groupMaxTracks")) this.set("isMaxTracksReached", true);
                    if (this.get("numberShownOnInterval") >= this.get("groupMaxTracks")) {
                        this.set("isMaxTracksReached", true);
                    }
                    else{
                        this.set("isMaxTracksReached", false);
                    }

                    this.dx.afterMove( [this.groupId], this.dx.browser.view.visibleTrackNames());
                }));
            },

            enableIsMaxTracksReachedWatcher: function() {
                return this.watch("isMaxTracksReached", lang.hitch(this, function(name, oldMtr, newMtr){
                        if (newMtr === true) this.hideSomeShownTracks();
                }));
            },


            enableToggleFeaturesWatcher: function(){
                return this.watch("toggleFeatures", lang.hitch(this, function(name, oldTogFeat, newTogFeat) {
                    if (typeof(newTogFeat) === "boolean" && newTogFeat === true) newTogFeat = "true";
                    else if (typeof(newTogFeat) === "boolean" && newTogFeat === false) newTogFeat = "false";
                    this.dxGroupCookie("toggleFeatures", newTogFeat);
                    this.dx.afterMove( [this.groupId], this.dx.browser.view.visibleTrackNames());
                }))
            },

            enableToggleSignalWatcher: function(){
                return this.watch("toggleSignal", lang.hitch(this, function(name, oldTogSig, newTogSig) {
                    if (typeof(newTogSig) === "boolean" && newTogSig === true) newTogSig = "true";
                    else if (typeof(newTogSig) === "boolean" && newTogSig === false) newTogSig = "false";
                    this.dxGroupCookie("toggleSignal", newTogSig);
                    this.dx.afterMove( [this.groupId], this.dx.browser.view.visibleTrackNames());
                }))
            },

            /* Watch the group tracks that are shown on the interval.
             ** Update the associated number (listened to by the widget)
             ** Trigger the update of the relevant shownTrackLabels */
            enableShownLabelsWatcher: function() {
                return this.watch("labelsShownOnInterval", lang.hitch(this, function(name, oldShown, newShown) {
                    this.set("numberShownOnInterval", newShown.length);
                    var relevantTrackLabels = this.get("relevantTrackLabels");
                    var shownRelevantTrackLabels = array.filter(relevantTrackLabels, lang.hitch(this, function(relevantLabel) {
                        if (array.indexOf(newShown, relevantLabel) !== -1) return relevantLabel;
                    }));
                    this.set("relevantShownTrackLabels", shownRelevantTrackLabels);
                }));
            },

            /* Watch the group numberShownOnInterval to check if the maximum
             ** number of tracks that the group can show is reached. */
            enableShownCountWatcher: function() {
                return this.watch("numberShownOnInterval", lang.hitch(this, function(name, oldCShown, newCountShown) {
                    if (newCountShown >= this.get("groupMaxTracks")) {
                    //if (newCountShown >= this.get("groupMaxTracks")) {
                        if (this.get("isMaxTracksReached") === false) {
                            this.set("isMaxTracksReached", true);
                            // this.set("groupTrackIdsToShow", []);
                        }
                    } else {
                        if (this.get("isMaxTracksReached") === true) {
                            this.set("isMaxTracksReached", false);
                        }
                    }
                }));
            },

            enableRelevantLabelsWatcher: function() {
                return this.watch("relevantTrackLabels", lang.hitch(this, function(name, oldRelevant, newRelevant) {
                    this.set("numberRelevantOnInterval", newRelevant.length);
                    var shownOnInterval = this.get("labelsShownOnInterval");
                    var shownRelevantTrackLabels = array.filter(newRelevant, lang.hitch(this, function(relevantLabel) {
                        if (array.indexOf(shownOnInterval, relevantLabel) !== -1) return relevantLabel;
                    }));
                    this.set("relevantShownTrackLabels", shownRelevantTrackLabels);
                }));
            },

            enableRelevantCountWatcher: function() {
                return this.watch("numberRelevantOnInterval", lang.hitch(this, function() {
                    this.set("groupAllShown", this.areRelevantTracksAllShown());
                }));
            },

            enableRelevantShownLabelsWatcher: function() {
                return this.watch("relevantShownTrackLabels", lang.hitch(this, function(name, oldRelevantShown, newRelevantShown) {
                    this.set("numberRelevantShownOnInterval", newRelevantShown.length);
                }));
            },

            enableRelevantShownCountWatcher: function() {
                return this.watch("numberRelevantShownOnInterval", lang.hitch(this, function() {
                    this.set("groupAllShown", this.areRelevantTracksAllShown());
                }));
            },

            areRelevantTracksAllShown: function() {
                // return (this.get("numberRelevantShownOnInterval") - this.get("numberRelevantOnInterval") >= 0 ? true : false);
                //return (this.get("numberRelevantShownOnInterval") - this.get("numberRelevantOnInterval") < 0 ? false : true);
                return ( this.get("numberRelevantOnInterval") -  this.get("numberRelevantShownOnInterval") > 0 ? false : true);
            },

            /*********************
            ------ TOPICS ------
            ----------------------------------------------------------------------------------------------------------------------------
            ** 2 topics are specific to dynamix: /dynamix/tracks/dx/show and /dynamix/tracks/dx/hide
            ** This topics are use to transfer promises that will confirm that the tracks have been added to
            ** the track container (or removed). This is done because tracks are added in a specific order
            ** that makes sense, and so once and add is trigger, the code integrate this information to
            ** locate the position of other tracks.
            -----------------------------------------------------------------------------------------------------------------------------
            *********************/

            /* Subscribe to a show tracks topic (managed by dynamix): /dynamix/tracks/dx/show
             ** This topic is used to retrieve promises of tracks that have been requested to be added to the
             ** track container. Once a promise has been published through the topic, this function will attempt
             ** call a promise resolution function at regular interval.
             ** It is resolved */
            enableDxTrackShowTopic: function() {
                /* Subscribe to the show dynamix topic. This topic is used to retrieve promises of tracks
                 ** that dynamix requested to be added to the view. Once a promise has been emitted, this
                 ** handler will attempt to resolve it at regular interval. It is resolved when the requested track
                 ** has been added to the track container. */
                return this.dx.browser.subscribe('/dynamix/tracks/dx/show/' + this.groupId, lang.hitch(this, function(showPromise, trackLabels) {
                    var resolveShownInterval = setInterval(lang.hitch(this, function() {
                        this.attemptResolveShowPromise(showPromise, trackLabels, resolveShownInterval);
                    }), 5);
                }));
            },
            /* attemptResolveShowPromise is not a *topic* per say but works in pair with enableDxTrackShowTopic
             ** It resolves promises once the tracks is found in the track container (groupVisibleLabels). */
            attemptResolveShowPromise: function(showPromise, trackLabels, resolveShownInterval) {
                var missingShown = [];
                var groupVisibleLabels = this.getShownOnIntervalLabelsList(this.dx.browser.view.visibleTrackNames());
                if (array.every(trackLabels, lang.hitch(this, function(dxLabel) {
                        // returns true if dxLabel in visibleLabel, false otherwise
                        return (array.indexOf(groupVisibleLabels, dxLabel) !== -1);
                    }))) {
                    /* array.every is shorcicuited asa. one dxLabel has not been found in the visibledxLabel
                     ** Oppositely it returns true if all have been found in view. The setInterval call is cleared
                     ** and the promise is resolved. */
                    clearInterval(resolveShownInterval);
                    this.set("labelsShownOnInterval", groupVisibleLabels);
                    this.set("idsShownOnInterval", this.getIdsFromLabelsList(this.get("labelsShownOnInterval")));
                    this.dx.browser.publish('/dynamix/tracks/dx/colorise/'+this.groupId, groupVisibleLabels);
                    showPromise.resolve(groupVisibleLabels);
                }
            },


            enableResetBarLoopTopic: function() {
                /* Subscribe to the show dynamix topic. This topic is used to retrieve promises of tracks
                 ** that dynamix requested to be added to the view. Once a promise has been emitted, this
                 ** handler will attempt to resolve it at regular interval. It is resolved when the requested track
                 ** has been added to the track container. */
                return this.dx.browser.subscribe('/dynamix/tracks/dx/loop/', lang.hitch(this, function() {
                    this.set("barLoop", false);
                }));
            },

            enableColoriseGroupTrackTopic: function() {
                /* Subscribe to the show dynamix topic. This topic is used to retrieve promises of tracks
                 ** that dynamix requested to be added to the view. Once a promise has been emitted, this
                 ** handler will attempt to resolve it at regular interval. It is resolved when the requested track
                 ** has been added to the track container. */
                return this.dx.browser.subscribe('/dynamix/tracks/dx/colorise/'+this.groupId, lang.hitch(this, function(groupVisibleLabels) {
                    if (!!this.groupColor){
                        array.forEach(groupVisibleLabels, lang.hitch(this, function(trackLabel){
                            // add class to node, dco not duplicate
                            //domClass.add(dom.byId("track_"+trackLabel), "tf_"+this.groupId);
                            var colTrack = dom.byId("track_"+trackLabel)
                            if (colTrack !== null) domClass.add(dom.byId("track_"+trackLabel), "tf_"+this.groupId)
                        }));
                    }
                    var myGroupTrackNodes = query(".tf_"+this.groupId)
                    if (myGroupTrackNodes.length > 0){
                        myGroupTrackNodes.style("border", "none")
                        myGroupTrackNodes.style("margin-top", "0px")
                        var myGroupTrackLabelNodes = query(".tf_"+this.groupId+" .track-label")
                        myGroupTrackLabelNodes.style("border-left", "6px solid "+this.groupColor)
                        domStyle.set(myGroupTrackNodes[0], "margin-top", "1px")
                        domStyle.set(myGroupTrackNodes[0], "border-top", "3px solid "+this.groupColor)
                        domStyle.set(myGroupTrackNodes[myGroupTrackNodes.length-1], "border-bottom", "2px solid "+this.groupColor)
                    }
                }));
            },

            // coloriseVisibleGroupTracks: function(groupVisibleLabels){
            //     /* COLOR TRACKS BACKGROUND */
            //     array.forEach(groupVisibleLabels, lang.hitch(this, function(trackLabel){
            //         if (array.indexOf(this.groupTrackLabelsList, trackLabel) !== -1){ // double check
            //             // domStyle.set("track_"+trackLabel, "background", this.groupColor ); // the whole track background
            //             domStyle.set("label_"+trackLabel, "border-left", "8px solid "+this.groupColor );
            //             // domStyle.set("label_"+trackLabel, "background", this.groupColor ); // label only
            //         }
            //     }));
            // },

            /* Tracks can be hidden either by the controller or by the view. When the view request tracks to be hidden,
             ** it comes from the user that clicked one of the UI buttons that allow to hide some tracks. In such case, a
             ** hidden track will still be relevant on the given interval, so it needs to be added back to the list of tracksToShow,
             ** e.g. in a case where the user want to put them back in (using the "+" button).
             ** To allow a better turn over of tracks when maxTracks is reached, the tracks that just have been hidden by
             ** the user will be added at the end of the relevantTracks list, and the trackToShow list is recomputed considering
             ** the new order. Tracks that have just been hidden will be the last tracka to be shown later. */
            enableDxWidgetHidTrackTopicListener: function() {
                return this.dx.browser.subscribe('/dynamix/tracks/dx/widget/hidden', lang.hitch(this, function(hiddenTracks, visibleDxLabels) {
                    this.set("labelsShownOnInterval", this.getGroupTracksFromList(visibleDxLabels));
                    this.set("idsShownOnInterval", this.getIdsFromLabelsList(this.get("labelsShownOnInterval")));
                }));
            },

            // TODO
            // HERE NEED JBROWSE UI HIDE TRACK (CLICK ON TRACK CROSS)
            // WITH DIFFERENT REACTION ACCORDING TO THE GROUP MODE:
            // MANUAL : DO NOTHING JUST ADD IT BACK TO END OF TRACK TO SHOW (SEE WHAT LIST USES ASYNC SHOW TRACKS)
            // AUTO: ADD NEXT THEN READD TRACK TO LIST
            enableDxUserHidTrackTopicListener: function() {
                return this.dx.browser.subscribe('/jbrowse/v1/v/tracks/hide', lang.hitch(this, function(hiddenTracksConfs) {
                    array.forEach(hiddenTracksConfs, lang.hitch(this, function(hiddenTracksConf) {
                        if (!hiddenTracksConf.hasOwnProperty("dynamix_id")){
                            return;
                        }
                        else {
                            var hidTrackLabel = hiddenTracksConf["label"];
                            if (array.indexOf(this.groupTrackLabelsList, hidTrackLabel) !== -1){
                                // track was relevant, put it back AT THE END
                                // else do not
                                var relevantArrIndex = array.indexOf(this.relevantTrackLabels, hidTrackLabel);
                                if (relevantArrIndex !== -1){
                                    this.relevantTrackLabels.splice(relevantArrIndex, 1);
                                    this.relevantTrackLabels.push(hidTrackLabel);
                                }
                                var shownOnIntervalTrackIndex = array.indexOf(this.labelsShownOnInterval, hidTrackLabel);
                                this.labelsShownOnInterval.splice(shownOnIntervalTrackIndex, 1);
                                this.set("labelsShownOnInterval", this.labelsShownOnInterval);
                                // remove it from the list of visible tracks
                                this.set("idsShownOnInterval", this.getIdsFromLabelsList(this.get("labelsShownOnInterval")));
                                // After a click on the cross if the group is in auto mode, do nothing
                                // Re show the track after a move!! Or after click on + button
                                // So recompute the list of TrackLabelsToShow
                                var visibleDxLabels = this.dx.visibleDxLabels();

                                // this.betterGetOrderedRelevantLabels(visibleDxLabels, this.get("relevantTrackLabels"));
                                this.betterGetOrderedRelevantLabels(visibleDxLabels, this.get("relevantTrackLabels"));
                                this.set("barLoop", false);
                            }
                        }
                    }));
                    this.dx.browser.publish('/dynamix/tracks/dx/colorise/'+this.groupId, this.labelsShownOnInterval);
                }));
            },


            // /* Subscribe to a hide tracks topic (managed by dynamix): '/dynamix/tracks/dx/core/hidden'
            // ** This topic is used to retrieve promises of tracks that have been requested to be removed from the
            // ** track container. Once a promise has been published through the topic, this function will attempt
            // ** call a promise resolution function at regular interval.
            // ** It is resolved when the track is not found in the track container (visibleDxLabels). */
            enableDxCoreHidTrackTopicListener: function() {
                return this.dx.browser.subscribe('/dynamix/tracks/dx/core/hidden', lang.hitch(this, function(hiddenTracks, visibleDxLabels) {
                    var groupVisibleLabels = this.getGroupTracksFromList(visibleDxLabels);
                    var groupVisibleIds = this.getIdsFromLabelsList(visibleDxLabels);
                    var groupTrackLabelsToHide = this.filterHiddenTrackLabelsOutFromList(this.trackLabelsToHide, hiddenTracks);
                    this.set("labelsShownOnInterval", groupVisibleLabels);
                    this.set("idsShownOnInterval", groupVisibleIds);
                    this.set("trackLabelsToHide", groupTrackLabelsToHide);
                    this.dx.browser.publish('/dynamix/tracks/dx/colorise/'+this.groupId, this.labelsShownOnInterval);
                }));
            },

            filterHiddenTrackLabelsOutFromList: function(trackListToFilter, hiddenTracks) {
                return array.filter(trackListToFilter, lang.hitch(this, function(t) {
                    // return it only if it's not in the hiddenTracks list
                    if (array.indexOf(hiddenTracks, t) === -1) return t;
                }));
            },


            /* A sensitivity threshold is define. It is a percentage based on the length pf the observed region.
             ** If after a move this threshold is passed, require an update of the view from dynamix
             ** To take into account the considered group overlap range, the groupRangeExtra length is added
             ** to the threshold */
            isMoveSignificant: function(genomicInterval) {

                var reachedSensivityThreshold = true;
                this.previousZoomLevel = this.currZoomLevel;
                this.currZoomLevel = this.dx.browser.view.curZoom;
                /* Require update if the reference seq (e.g. chromosome) name has changed
                 ** or the zoom level has changed */
                if (this.significantMoveGenomicInterval['ref'] != genomicInterval['ref'] ||
                    this.previousZoomLevel != this.currZoomLevel) {
                    return true;
                }
                var lastVisibleOverThresholdStart = this.significantMoveGenomicInterval['start'];
                var lastVisibleOverThresholdEnd = this.significantMoveGenomicInterval['end'];
                var genomicIntervalStart = genomicInterval.start;
                var genomicIntervalEnd = genomicInterval.end;
                var startDifference = Math.abs(parseInt(lastVisibleOverThresholdStart, 10) - parseInt(genomicIntervalStart, 10));
                var endDifference = Math.abs(parseInt(lastVisibleOverThresholdEnd, 10) - parseInt(genomicIntervalEnd, 10));
                // Compute the move length percentage
                var overallDifference = (startDifference + endDifference) / 2;
                var genomicIntervalLength = parseInt(genomicInterval['end'], 10) - parseInt(genomicInterval['start'], 10);
                var approxCoarseMoveLength_perCent = ((100 * overallDifference) / genomicIntervalLength);
                var consideredOnseSidedGroupRange = this.get("groupRange") || 0;
                /* Consider the overlap range in the threshold */
                return (approxCoarseMoveLength_perCent > this.dx.movementSensitivityThreshold + consideredOnseSidedGroupRange);
            },

            moveIsSignificant: function(genomicInterval){
                this.significantMoveGenomicInterval = genomicInterval;
                this.set("numberRelevantOnInterval", 0); // Dont know what is gonna be relevant so: reset to 0
                this.set("barLoop", false);
            },

            computeGroupExtraConsideredVisibleOneSideLength: function(genomicInterval) {
                var genomicIntervalLength = genomicInterval.end - genomicInterval.start;
                var consideredOnseSidedGroupRange = this.get("groupRange") || 0;
                return (consideredOnseSidedGroupRange * genomicIntervalLength) / 100;
            },
            /* Modify current View Info (observed region) with +/- (overlap_range * visible region)*/
            applyGroupRangeParameter: function(genomicInterval) {
                /* It is really important to clone the original object, since if it is not done, the *references* to
                 ** the values inside the genomicInterval object (itself passed as value, but with references inside)
                 ** will be changed, when it's actuall yimportant to be able to keep the original genomicInterval (e.g.
                 ** for computing the sensitivity threshold)*/
                genomicInterval = lang.clone(genomicInterval);
                var extraConsideredOneSideLength = this.computeGroupExtraConsideredVisibleOneSideLength(genomicInterval);
                var newEnd = parseInt(genomicInterval.end, 10) + parseInt(extraConsideredOneSideLength, 10); // 10 stands for *base 10*
                var newStart = parseInt(genomicInterval.start, 10) - parseInt(extraConsideredOneSideLength, 10);
                /* Check upper and lower boundaries */
                if (newEnd > this.dx.browser.view.ref.length) {
                    newEnd = this.dx.browser.view.ref.length;
                }
                if (newStart < 1) {
                    newStart = 1;
                }
                if (newEnd > newStart && (genomicInterval.end != newEnd || genomicInterval.start != newStart)) {
                    genomicInterval.end = newEnd;
                    genomicInterval.start = newStart;
                }
                return genomicInterval;
            },


            asyncGetTrackIdsFromFeatures: function(genomicInterval) {
                /* Query our group dynamix track for features. Each features is one or more dynamix track IDs we want to
                 ** add to the track container */
                var groupVisibleRegion = this.applyGroupRangeParameter(genomicInterval);
                var getFromFeatDeferred = new Deferred();
                //
                var relevantTrackIdsList = [];
                var groupTrackIdsList = this.get("groupTrackIdsList");
                // this.groupTrackStore = this.dx.getTrackStore(this.groupTrackName);
                this.dx.asyncGetTrackStore(this.groupTrackName).then(lang.hitch(this, function(groupTrackStore) {
                    this.trackStoreIsNotEmpty(groupTrackStore, groupVisibleRegion['ref']).then(lang.hitch(this, function(isNotEmpty) {
                        groupTrackStore.getFeatures(groupVisibleRegion,
                        /* callback for each feature found on groupVisibleRegion*/
                        lang.hitch(this, function(feat) {
                            // getFromFeatDeferred.progress();
                            dxidsList = feat.get("Name").split(",");
                            for (i = 0; i < dxidsList.length; i++) {
                                var dxid = dxidsList[i];
                                if (array.indexOf(groupTrackIdsList, dxid) === -1) {
                                    continue;
                                }
                                // else if (array.indexOf(relevantTrackIds, dxid) === -1){ // we do uniqify instead
                                relevantTrackIdsList.push(dxid);
                                // }
                            }
                        }),
                        /* afterend callback */
                        lang.hitch(this, function() {
                            // groupTrackIdsToShow = this.filterGroupTrackIds(groupTrackIdsToShow);
                            // the previous step should return a list ordered according to the groupIds
                            // so as defined by user (sorting once at init of group).
                            // this.groupTrackIdsToShow = groupTrackIdsToShow;
                            relevantTrackIdsList = Util.uniq(relevantTrackIdsList)
                            relevantTrackIdsList = this.orderGroupTrackIdsList(relevantTrackIdsList);
                            this.set("relevantTrackIdsList", relevantTrackIdsList);
                            getFromFeatDeferred.resolve(relevantTrackIdsList);
                            // return getFromFeatDeferred.promise;
                        }),
                        /* error callback */
                        function(error) {
                            // somehow never occured during dynamix development...
                            getFromFeatDeferred.resolve([]);
                        });
                    }), lang.hitch(this, function(err) {
                        this.set("numberShownOnInterval", 0);
                        this.set("numberRelevantOnInterval", 0);
                        this.set("numberRelevantShownOnInterval", 0);
                        var visTrack = this.dx.browser.view.visibleTrackNames();
                        var groupVisTrack = this.getShownOnIntervalLabelsList(visTrack);
                        this.hideShownTracks(groupVisTrack);
                        this.set("relevantTrackIdsList", []);
                        getFromFeatDeferred.resolve([]);
                    }));
                }), lang.hitch(this, function(getStoreErr) {
                    getFromFeatDeferred.resolve([]);
                }));
                return getFromFeatDeferred.promise;
            },

            /* This is a "hack". It's a copy/paste piece of JBrowse code that dynamix call before JBrowse itself
             ** This is to avoid to call getFeatures if it's gonna return an error that is gonna make our code
             ** execution stop ... */
            trackStoreIsNotEmpty: function(groupTrackStore, refName) {
                var fetchBefore = new Deferred();
                if (groupTrackStore.hasOwnProperty('empty')) {
                    if (groupTrackStore['empty']) return fetchBefore.resolve(true);
                }
                var url = groupTrackStore.resolveUrl(
                    groupTrackStore._evalConf(groupTrackStore.urlTemplates.root), {
                        refseq: refName
                    }
                );
                var thisB = this.dx.browser;
                xhr.get(url, {
                    handleAs: 'json',
                    failOk: true
                }).then(function() {
                    fetchBefore.resolve(true);
                }, function() {
                    fetchBefore.reject(false);
                });
                return fetchBefore;
            },

            orderGroupTrackIdsList: function(trackIds) {
                // iterate on groupTrackIds to follow the specified order
                trackIds = array.filter(this.get("groupTrackIdsList"), function(t) {
                    if (array.indexOf(trackIds, t) !== -1) return t;
                });
                return trackIds;
            },


            getTrackLabelsToHide: function(visibleDxLabels, relevantTrackLabels) {
                /* Filter the shown track to find those that should not be shown anymore  */
                var getToHideDeferred = new Deferred();
                var trackLabelsToHide = Util.uniq(array.filter(visibleDxLabels, lang.hitch(this, function(label) {
                    if (array.indexOf(relevantTrackLabels, label) === -1) return label;
                })));
                this.set("trackLabelsToHide", trackLabelsToHide);
                if (this.get("groupMode") === "manual" || this.get("groupMode") === "disabled") getToHideDeferred.resolve([]);
                else getToHideDeferred.resolve(trackLabelsToHide);
                return getToHideDeferred.promise;
            },

            getRelevantTrackLabelsFromRelevantTrackIds: function() {
                var relevantTrackLabels = this.getLabelsFromIdsList(this.relevantTrackIdsList);
                /* Filter feature / signal if needed */
                if ((!this.toggleFeatures || !this.toggleSignal) || (!this.toggleFeatures && !this.toggleSignal)) {
                    relevantTrackLabels = this.filterRelevantTrackLabelsByCategory(relevantTrackLabels);
                }
                this.set("relevantTrackLabels", relevantTrackLabels);
                this.set("relevantTrackIds", this.getIdsFromLabelsList(relevantTrackLabels));
                return relevantTrackLabels;

            },

            /* Filter the set of relevant tracks according to the track type (signal | features | both)
             ** to take into consideration what the user wants to see */
            filterRelevantTrackLabelsByCategory: function(relevantTrackLabels) {
                return array.filter(relevantTrackLabels, lang.hitch(this, function(tLabel) {
                    var trackConf = this.dx.browser.trackConfigsByName[tLabel] || {};
                    var trackCategory = trackConf['category'] || "";
                    if (trackCategory.toLowerCase().trim() === 'signal' && this.toggleSignal) return tLabel;
                    if (trackCategory.toLowerCase().trim() === ('features' || 'feature') && this.toggleFeatures) return tLabel;
                }));
            },


            hideShownTracks: function(labelsToHide) {
                // requested from the user!
                // this.set("groupTrackIdsToShow", []); //resetting
                return this.dx.viewHideTracks(labelsToHide).then(
                        // This basically reset the track order when user toggled and untoggled showTracks or showFeatures
                        // This allow to restart from a mix of both signal and features
                        lang.hitch(this, function(visibleDxLabels){
                            var relevantTrackLabels = this.getRelevantTrackLabelsFromRelevantTrackIds();
                            // selectTrackLabelToShow
                            this.betterGetOrderedRelevantLabels(visibleDxLabels, relevantTrackLabels);
                            return this.dx.newResolvedDummyPromise();
                        })
                );
            },

            hideSomeShownTracks: function() {
                if (this.get("isMaxTracksReached") === true){
                //if (this.get("isMaxTracksReached") === true || ( this.get("numberShownOnInterval") - this.get("groupMaxTracks") > 0 ) ){
                    var numberToHide = this.get("numberShownOnInterval") - this.get("groupMaxTracks")
                    if (numberToHide > 0){
                        var someTracksToHideLabel = this.labelsShownOnInterval.slice(-numberToHide);
                        this.hideShownTracks(someTracksToHideLabel);
                    }
                }
            },

            showMultipleTracks: function() {
                /* Show more tracks. Used to "update" the track shown from a group,
                 ** after a click on the correponding group "plus" button. */
                return this.betterSeqShowTracks(this.get("labelsShownOnInterval"));
                // return this.asyncShowTracks();
            },

            /*
            Show + Hide tracks. Used to "refresh" the track shown from a group,
            ** after a click on the correponding group "refresh" button.
            */
            showAndHideMultipleTracks: function() {
                /*
                 ** First, the tracks that are really no more relevant are hidden.
                 ** In the automatic mode, this should already have happened. And therefore
                 ** this.trackLabelsToHide should be empty (updated when hide promise is resolved)!
                 ** But not in manual mode! */
                return this.dx.controllerHideTracks(this.get("trackLabelsToHide")).then(lang.hitch(this, function() {
                    if (!this.get("groupAllShown")) {
                        if (this.get("isMaxTracksReached") ||
                            this.dx.dxMasterWidget.get("totalShownNumber") >= this.dx.totalDxMaxTracks )
                        {
                             /* The groupMaxTracks has been reached already, but the user click refresh to display the relevant
                             ** tracks that are not shown. Hide some *relevant* tracks, to show other *relevant* tracks */
                            /* Determine how many tracks should be hidden to show the next relevant ones.
                             ** Since groupMaxTracks is reached, it has to be maximum this number */

                            var numberRemainingToShow = this.get("numberRelevantOnInterval") - this.get("numberShownOnInterval");
                            var maxTurnoverNumber = Math.ceil(this.get("groupMaxTracks")/2);
                            var turnoverNumber = Math.min(numberRemainingToShow, maxTurnoverNumber);
                            var groupVisibleDxLabels = this.getShownOnIntervalLabelsList(this.dx.browser.view.visibleTrackNames());
                            var tracksToHide = groupVisibleDxLabels.slice(0, turnoverNumber);
                            /* The view will trigger the hide event */
                            return this.dx.viewHideTracks(tracksToHide).then(lang.hitch(this, function(vTrackLabels){
                                var groupVisibleDxLabels =  this.getShownOnIntervalLabelsList(vTrackLabels);
                                return this.betterSeqShowTracks(groupVisibleDxLabels);
                            }));
                        } else {
                            /* The group Max Track is not reached, normally show other missing relevant tracks */
                            return  this.showMultipleTracks();
                        }
                    }
                    else{
                        return this.dx.newResolvedDummyPromise(this.dx.browser.view.visibleTrackNames());
                    }
                }));
            },

            betterGetOrderedRelevantLabels: function(visibleDxLabels, relevantTrackLabels) {
                /* DO NOT FILTER THE LIST BUT
                ** Have a consitent order compared to what is shown. Always start with the first shown track */
                var orderedRelevantTrackLabels = this.orderTrackLabelsToShow(visibleDxLabels, relevantTrackLabels);
                // var trackLabelsToShow = this.filterAlreadyShownLabels(visibleDxLabels, relevantTrackLabels);
                this.set("relevantTrackLabels", orderedRelevantTrackLabels);
                this.set("relevantTrackIds", this.getIdsFromLabelsList(this.relevantTrackLabels));
                return orderedRelevantTrackLabels;
            },

            orderTrackLabelsToShow: function(visibleDxLabels, relevantTrackLabels){
                var relevantTrackLength = relevantTrackLabels.length;
                if (this.get("barLoop") === false) return relevantTrackLabels;
                if (relevantTrackLength < 2) return relevantTrackLabels;
                else{
                    var visibleRelevantTrackLabel;
                    var visibleRelevantTrackLabelIndex = -1;
                    for (var itRelev=0; itRelev < relevantTrackLength; itRelev++){
                        visibleRelevantTrackLabel = relevantTrackLabels[itRelev];
                        if (array.indexOf(visibleDxLabels, visibleRelevantTrackLabel) !== -1) {
                            visibleRelevantTrackLabelIndex = itRelev;
                            break;
                        }
                    }
                    if (visibleRelevantTrackLabelIndex !== -1){
                        var listBeginning = relevantTrackLabels.splice(0, visibleRelevantTrackLabelIndex);
                        this.relevantTrackLabels = relevantTrackLabels.concat(listBeginning);
                        return this.relevantTrackLabels;
                    }
                    else{
                        return relevantTrackLabels;
                    }
                }
            },

            selectTrackLabelToShow: function(groupVisibleTrackLabels, orderedRelevantTrackLabels){
                /*What to do:
                ** have the list of visible tracks + the lsit of relevant racks containing visible tracks
                ** we get the [0, maxtacks] beginning of list tracktoshow
                ** from this list we hide all the tracks that are not in that sublist [0, maxtacks]
                ** show the rest */
                var trackLabelsToShow = []
                var orderedRelevantTrackLabelsLength = orderedRelevantTrackLabels.length;
                var nTrackToShow = 0;
                for (var iterTrack = 0; iterTrack < orderedRelevantTrackLabelsLength ; iterTrack++){
                    if (nTrackToShow === this.get("groupMaxTracks")) break;
                    var testTrackToShow = orderedRelevantTrackLabels[iterTrack];
                    if (array.indexOf(groupVisibleTrackLabels, testTrackToShow) === -1 ){
                        nTrackToShow++;
                        trackLabelsToShow.push(testTrackToShow);
                    }
                }
                return trackLabelsToShow;
            },




            betterSeqShowTracks: function(groupVisibleTrackLabels) {
                var orderedRelevantTrackLabels = this.betterGetOrderedRelevantLabels(groupVisibleTrackLabels, this.get("relevantTrackLabels"));
                var fewerTrackLabelsToShow = this.selectTrackLabelToShow(groupVisibleTrackLabels, orderedRelevantTrackLabels);

                var originalGroupMode = this.get("groupMode");
                var showTrackPromise = this.dx.newResolvedDummyPromise(groupVisibleTrackLabels);
                var arrayOfShowPromises = [ showTrackPromise ];
                array.forEach(fewerTrackLabelsToShow, lang.hitch(this, function(nextTrackToShow){
                    showTrackPromise = showTrackPromise.then(lang.hitch(this, function(groupVisibleLabels){
                        /* First IF In case the group mode changes while showing.
                        ** TODO A better option could be to register the current promise in a global variable
                        ** that can be accessed by the function that react to gorup mode change.
                        ** This DOES NOT prevent the last promise to be executed till the end
                        ** i.e. if while dynamix loads tracks user set up the group to "disabled" it will hides
                        ** all tracks except the last one....*/
                        if (this.get("groupMode") !== originalGroupMode
                            && this.get("groupMode") !== "automatic")
                        {
                            var asyncShowNextTrackPromise = this.dx.newResolvedDummyPromise(groupVisibleLabels);
                        }
                        else{
                            var asyncShowNextTrackPromise = this.asyncShowNextTrack(nextTrackToShow, groupVisibleLabels);
                        }
                        arrayOfShowPromises.push(asyncShowNextTrackPromise);
                        return asyncShowNextTrackPromise;
                    }));
                }));
                return showTrackPromise.then(lang.hitch(this, function(){
                    return pall(arrayOfShowPromises).then(lang.hitch(this, function(showPromisesResults){
                        /* Each showPromisesResults return value is the list of visible tracks (for the group)
                        ** As tracks are added one by one, the list of visible tracks of interest is the last one*/
                        var lastVisibleList = showPromisesResults.pop();
                        return this.dx.newResolvedDummyPromise(lastVisibleList);
                    }));
                }));
            },

            asyncShowNextTrack: function(trackLabel, groupVisibleLabels){
                // If group all shown this should not be called anyway! (double check)
                if (!this.get("groupAllShown") &&
                    this.get("numberShownOnInterval") < this.get("groupMaxTracks") &&
                    this.dx.dxMasterWidget.get("totalShownNumber") < this.dx.totalDxMaxTracks )
                {
                    return this.injectTrackInContainer(trackLabel, groupVisibleLabels);
                } else {
                    this.set("barLoop", true);
                    /* When group or global maxTracks is reached. TrackLAbel cannot be added,
                    ** However, it already has been shifted out the list of track labels to show, so
                    ** it is put back in !  */
                    /* In the meantime since we want to go on showing next groups, a dummy
                    ** resolved promise is returned */
                    return this.dx.newResolvedDummyPromise(this.dx.browser.view.visibleTrackNames());
                }
            },

            injectTrackInContainer: function(trackLabel, groupVisibleLabels) {
                /* Try to find an anchor in the track containerfor *trackLabel*, that is
                 ** a track to be used as a position reference in the container.
                 ** If it finds one, call insertTracks. If not call showTracks
                 ** Returns the promise that, when resolved, certify that the
                 ** added track is in the track container (not on its way to, lost somewhere). */
                var showPromise;
                var trackID = this.getIdFromLabel(trackLabel);
                anchorInfo = this.findTrackAnchorInContainer(trackID, trackLabel, groupVisibleLabels);
                var anchorLabel = anchorInfo[0] || false;
                var beforeAnchor = anchorInfo[1] || false;
                if (!!anchorLabel) {
                    return this.dx.insertTracks(this.groupId, trackLabel, anchorLabel, beforeAnchor);
                } else {
                    return this.dx.showTracks(this.groupId, trackLabel);
                }
            },

            findTrackAnchorInContainer: function(trackId, trackLabel, groupVisibleLabels) {
                /* If group shows some track, find an anchor within thos shown tracks
                 ** If the group has no track visible, try to find an anchor in other groups */
                var anchorInfo = [undefined, false]; // [ anchorId (string), beforeAnchor (bool)]
                // var groupVisibleLabels = this.get("groupVisibleLabels");
                if (groupVisibleLabels.length > 0) {
                    anchorInfo = this.findAnchorInfoThisGroup2(trackId, trackLabel, groupVisibleLabels);
                } else {
                    anchorInfo = this.findAnchorInfoOtherGroups();
                }
                /* returns anchorInfo = [ undefined, false ] if no tracks are shown at all*/
                return anchorInfo;
            },


            /* Find an anchor in this group
            ** We check where the track is in the ordered relevant track IDs and insert it right after/before
            ** which ever first is found in view. if the anchor is ordered before then the track is inserted after
            ** and vice versa.
            //
            /** FIXME:
             ** It might be better if the features and signal REALLY share the same dynamix ID.
             ** This function would be broken, since it does not really rely on the "dynamix_priority"
             ** field itself but on the suffix (which local compare is able to sort properly...)
             ** If all the related tracks share the same dynamix ID, then the findAnchor functions
             ** will have to not use localeCompare but to use the "dynamix_priority" track config tag
             ** to order tracks. That is, when localeCompare give 0 (exact same ID), use the tag.
             */
            findAnchorInfoThisGroup2: function(trackId, trackLabel, labelsShownOnInterval) {
                var idsShownOnInterval = this.get("idsShownOnInterval");
                var relevantTrackIds = this.get("relevantTrackIds");
                var pivotIndex = array.indexOf(relevantTrackIds, trackId);
                var trackBeforePivot = relevantTrackIds.slice(0, pivotIndex);
                var trackAfterPivot = relevantTrackIds.slice(pivotIndex, relevantTrackIds.length);
                trackBeforePivot.reverse();
                var sortedRelevantTrackIds = trackBeforePivot.concat(trackAfterPivot);
                var sortedRelevantTrackIdsLength = sortedRelevantTrackIds.length;

                for (var i = 0; i <  sortedRelevantTrackIdsLength; i++) {

                    var pAnchorId = sortedRelevantTrackIds[i];
                    if (array.indexOf(idsShownOnInterval, pAnchorId) !== -1){ // potential anchor id IS shown
                        if(i < pivotIndex){ // potential anchor id IS BEFORE pivot
                            // !!! SPECIAL CASE !!!
                            if (pAnchorId === idsShownOnInterval[idsShownOnInterval.length-1]
                                && idsShownOnInterval[0].lastIndexOf(trackId, 0) === 0)
                            {
                                return [this.findAssociatedLabelsAnchorInContainer(idsShownOnInterval[0], true, labelsShownOnInterval), true];
                            }
                            return [this.findAssociatedLabelsAnchorInContainer(pAnchorId, false, labelsShownOnInterval), false ];
                        }
                        else if(i >= pivotIndex){ // potential anchor id is AFTER pivot
                            return [this.findAssociatedLabelsAnchorInContainer(pAnchorId, true, labelsShownOnInterval), true ];
                        }
                    }
                }
                return [undefined, false];
            },

            findAssociatedLabelsAnchorInContainer: function(anchorId, beforeAnchorBool, labelsShownOnInterval) {
                var anchorAssociatedLabels = this.getLabelsFromIdSortedByPriority(anchorId);
                var anchorLabel = anchorAssociatedLabels[0]; // default
                if (anchorAssociatedLabels.length > 1) {
                    if (beforeAnchorBool === false) { // iterate forward, find first in view
                        for (var i = anchorAssociatedLabels.length - 1; i >= 0; i--) {
                            anchorLabel = anchorAssociatedLabels[i];
                            if (array.indexOf(labelsShownOnInterval, anchorLabel) !== -1) {
                                return anchorLabel;
                            }
                        }
                    } else if (beforeAnchorBool === true) { // iterate backward, find last in view
                        for (var j = 0; j < anchorAssociatedLabels.length; j++) {
                            anchorLabel = anchorAssociatedLabels[j];
                            if (array.indexOf(labelsShownOnInterval, anchorLabel) !== -1) {
                                return anchorLabel;
                            }
                        }
                    }
                }
                return anchorLabel;
            },

            findAssociatedLabelsAnchorInfo: function(trackId, trackLabel) {
                /* This is to decipher between two tracks with the same dynamix ID
                 ** It happens when both XYplot and Heatmap are configured for signal
                 ** Both can be ordered thanks to their priority tag */
                var anchorLabel;
                var beforeAnchor;
                var sortedLabelsList = this.getLabelsFromIdSortedByPriority(trackId);
                if (array.indexOf(sortedLabelsList, trackLabel) === 0) {
                    // its the first prioritised track, that is the XYplot
                    // so the id of the one already in container should be
                    // the id of sortedLabelsList[1]
                    beforeAnchor = true;
                    anchorLabel = sortedLabelsList[1];
                } else if (array.indexOf(sortedLabelsList, trackLabel) === 1) {
                    // the id of the one already in container should be
                    // the id of sortedLabelsList[0]
                    beforeAnchor = false;
                    anchorLabel = sortedLabelsList[0];
                }
                return [anchorLabel, beforeAnchor];
            },

            findAnchorInfoOtherGroups: function() {
                /* NB: We return a bool like dojo uses in the insertNodes function parameters for the trackDndWidget
                 ** to insert the track before/after the anchor */
                var anchorInfo = [undefined, false];
                if ( !! this.get("groupDefaultAnchor") ){
                    var groupsSharingSameAnchorLabel = this.filterGroupIdsSharingSameAnchorLabel(this.orderedGroupStackIds);
                    var groupsSharingSameAnchorPoint = this.filterGroupIdsSharingSameAnchorPoint(groupsSharingSameAnchorLabel);
                    // check the exact same anchro point (LABEL + BEFORE)
                    anchorInfo = this.findAnchorInfoOtherGroups2(groupsSharingSameAnchorPoint);
                    if (typeof anchorInfo[0] === "undefined") anchorInfo = [this.get("groupDefaultAnchor"), this.get("groupDefaultBeforeAnchor")];
                    if (typeof anchorInfo[0] === "undefined"
                        || array.indexOf(this.dx.browser.view.visibleTrackNames(), anchorInfo[0]) === -1)
                    {
                        anchorInfo = this.findAnchorInfoOtherGroups2(groupsSharingSameAnchorLabel);
                    }
                }
                // if still nothing, look in all the other groups
                if (typeof anchorInfo[0] === "undefined"){
                    anchorInfo = this.findAnchorInfoOtherGroups2(this.filterGroupIdsWithNoAnchorLabel(this.orderedGroupStackIds));
                }
                return anchorInfo;
            },

            filterGroupIdsSharingSameAnchorPoint: function(orderedGroupStackIds){
                var knownGroupsIdsWithSameAnchorPoint = [];
                var groupStackLength = orderedGroupStackIds.length;
                for (i = 0; i <= groupStackLength - 1; i++){
                    var otherGroupId = orderedGroupStackIds[i];
                    if (this.dx.dxTrackGroups.hasOwnProperty(otherGroupId)) {
                        var otherGroup = this.dx.dxTrackGroups[otherGroupId];
                        if (otherGroup.get("groupDefaultAnchor") === this.get("groupDefaultAnchor")
                            && otherGroup.get("groupDefaultBeforeAnchor") === this.get("groupDefaultBeforeAnchor"))
                        {
                            knownGroupsIdsWithSameAnchorPoint.push(otherGroupId);
                        }
                    }
                }
                return knownGroupsIdsWithSameAnchorPoint;
            },

            filterGroupIdsSharingSameAnchorLabel: function(orderedGroupStackIds){
                var knownGroupsIdsWithSameAnchorLabel = [];
                var groupStackLength = orderedGroupStackIds.length;
                for (i = 0; i <= groupStackLength - 1; i++){
                    var otherGroupId = orderedGroupStackIds[i];
                    if (this.dx.dxTrackGroups.hasOwnProperty(otherGroupId)) {
                        var otherGroup = this.dx.dxTrackGroups[otherGroupId];
                        if (otherGroup.get("groupDefaultAnchor") === this.get("groupDefaultAnchor")){
                            knownGroupsIdsWithSameAnchorLabel.push(otherGroupId);
                        }
                    }
                }
                return knownGroupsIdsWithSameAnchorLabel;
            },

            filterGroupIdsWithNoAnchorLabel:function(orderedGroupStackIds){
                var knownGroupIdsWithNoAnchorLabel = [];
                var groupStackLength = orderedGroupStackIds.length;
                for (i = 0; i <= groupStackLength - 1; i++){
                    var otherGroupId = orderedGroupStackIds[i];
                    if (this.dx.dxTrackGroups.hasOwnProperty(otherGroupId)) {
                        var otherGroup = this.dx.dxTrackGroups[otherGroupId];
                        if (! otherGroup.get("groupDefaultAnchor")){
                            knownGroupIdsWithNoAnchorLabel.push(otherGroupId);
                        }
                    }
                }
                return knownGroupIdsWithNoAnchorLabel;
            },


            findAnchorInfoOtherGroups2: function(orderedGroupIdsList){ // full list or shorten to only the one sharing anchors
                var pivotIndex = array.indexOf(orderedGroupIdsList, this.groupId);
                var idsBeforePivot = orderedGroupIdsList.slice(0, pivotIndex);
                var idsAfterPivot = orderedGroupIdsList.slice(pivotIndex, orderedGroupIdsList.length);
                idsBeforePivot.reverse();
                var otherGroupIds = idsBeforePivot.concat(idsAfterPivot);
                var otherGroupIdsCount = otherGroupIds.length;
                for (var i = 0; i < otherGroupIdsCount; i++) {
                    if (this.dx.dxTrackGroups.hasOwnProperty(otherGroupIds[i])) {
                        var otherGroup = this.dx.dxTrackGroups[otherGroupIds[i]];
                        // var last = arr[arr.length - 1];
                        if (otherGroup.get("numberShownOnInterval") > 0){
                            var shownLabelsList = otherGroup.get("labelsShownOnInterval");
                            if ( i < pivotIndex ){ // after last
                                return [shownLabelsList[shownLabelsList.length - 1], false]
                            }
                            else if ( i > pivotIndex ){ // before first
                                return [shownLabelsList[0], true]
                            }
                        }
                    }
                }
                return [undefined, false]
            },

            getShownOnIntervalLabelsList: function(visibleTrackNames) {
                // Return groupVisibleLabels
                return array.filter(visibleTrackNames, lang.hitch(this, function(t) {
                    if (array.indexOf(this.groupTrackLabelsList, t) !== -1) return t;
                }));
            },

            getGroupTracksFromList: function(trackNamesList) {
                // return only the tracks of this group
                return array.filter(trackNamesList, lang.hitch(this, function(t) {
                    if (array.indexOf(this.groupTrackLabelsList, t) !== -1) return t;
                }));
            },

            getLabelsFromId: function(dxid) {
                /* 1 ID corresponds 1 or more labels */
                if (this.groupDxidsToLabelMap.hasOwnProperty(dxid)) {
                    return Object.keys(this.groupDxidsToLabelMap[dxid]);
                }
                return [];
            },


            getSortedKeys: function(dic) {
                var keys = Object.keys(dic);
                return keys.sort(function(a, b) {
                    return dic[a] - dic[b];
                });
            },

            getLabelsFromIdSortedByPriority: function(dxid) {
                /* Returns a list of labels ordered by priority. This list will be 1 element if
                 ** we consider a feature file. But 2 ordered elements if we consider a signal file
                 ** assuming that the user requested both types of signal XYplot + density map */
                var sortedLabels = this.getSortedKeys(this.groupDxidsToLabelMap[dxid]);
                return sortedLabels;
            },

            // BUGGY AND I DONT KNOW WHY ???
            getLabelsFromIdsList: function(dxidsList) {
                var labels = [];
                array.forEach(dxidsList, lang.hitch(this, function(trackId) {
                    var associatedLabels = this.getLabelsFromId(trackId);
                    labels = labels.concat(associatedLabels);
                }));
                return labels;
            },

            getIdFromLabel: function(label) {
                // 1 label == 1 ID
                if (this.groupLabelsToDxidsMap.hasOwnProperty(label)) {
                    return this.groupLabelsToDxidsMap[label];
                }
                return "";
            },

            getIdsFromLabelsList: function(labelsList) {
                var idsList = [];
                array.forEach(labelsList, lang.hitch(this, function(tLabel) {
                    var gotId = this.getIdFromLabel(tLabel);
                    if (gotId !== "") {
                        idsList.push(gotId);
                    }
                }));
                return Util.uniq(idsList);
            },


            dxGroupCookie: function(dxGroupKey, value) {
                // store or recover a cookie for the group, using in fact localStorage implemented in browser
                dxGrpCookie = this.dx.dxCookie(this.groupId + "-" + dxGroupKey, value);
                // if (typeof dxGrpCookie === 'object'){
                try {
                    dxGrpCookie = dojo.fromJson(dxGrpCookie);
                } catch (err) {
                    /*Error occurs when the cookie is a string and the fromJson
                     ** function is called with that string. We want a string so it is returned*/
                }
                return dxGrpCookie;
            },


            removeDxGroupCookie: function(dxGroupKey) {
                return this.dx.removeDxCookie(this.groupId + "-" + dxGroupKey);
            },

            constructGroupGonfigDom: function() {
                var thisDxGroupDiv = domConstruct.create("div", {
                    class: "dxGroupConf"
                });
                var thisGroupTitle = this.dx.createTextDiv(this.groupId, "dxGroupTitle");
                thisDxGroupDiv.appendChild(thisGroupTitle);

                var thisDxGroupConfigParams = domConstruct.create("div", {
                    class: "dxGroupConfParams"
                });
                var thisDxGroupParamList = domConstruct.create("ul");

                var groupRangeHash = {};
                var groupModeHash = {};
                var groupMaxTracksHash = {};

                for (var parameter in this.parameterHash) {
                    if (this.parameterHash.hasOwnProperty(parameter)) {
                        var dxGroupParamName = this.parameterHash[parameter];
                        var dxGroupParamValue = this.get(dxGroupParamName);
                        var thisDxParamItem = domConstruct.create("li");
                        var thisParamTitle = this.dx.createTextDiv(parameter, "dxConfParamName");
                        thisDxParamItem.appendChild(thisParamTitle);
                        if (dxGroupParamName == 'groupRange') {
                            thisParamTitle.innerHTML = thisParamTitle.innerHTML + " (%)";
                            var groupRangeTextbox = this.dx.createNumberTextBox('p_range_' + this.groupId, dxGroupParamValue, 100, "dxRangeTextBox");
                            thisDxParamItem.appendChild(groupRangeTextbox.domNode);
                        } else if (dxGroupParamName == 'groupMaxTracks') {
                            var groupMaxTracksTextbox = this.dx.createNumberTextBox('p_maxTracks_' + this.groupId, dxGroupParamValue, 200, "dxRangeTextBox");
                            thisDxParamItem.appendChild(groupMaxTracksTextbox.domNode);
                        } else if (dxGroupParamName == 'groupMode') {
                            var groupModeSelect = this.createSelectDropdown('p_mode_' + this.groupId, dxGroupParamValue);
                            thisDxParamItem.appendChild(groupModeSelect.domNode);
                        } else if (dxGroupParamName == 'toggleFeatures') {
                            var groupFeatSelect = this.createSelectFilterDropdown('p_filterfeat_' + this.groupId, dxGroupParamValue);
                            thisDxParamItem.appendChild(groupFeatSelect.domNode);
                        } else if (dxGroupParamName == 'toggleSignal') {
                            var groupSignalSelect = this.createSelectFilterDropdown('p_filtersignal_' + this.groupId, dxGroupParamValue);
                            thisDxParamItem.appendChild(groupSignalSelect.domNode);
                        } else if (dxGroupParamName == 'groupTrackLabelsList') {
                            if (typeof dxGroupParamValue == 'string') dxGroupParamValue = dxGroupParamValue.split(',');
                            thisParamTitle.innerHTML = "Track Labels (" + dxGroupParamValue.length.toString() + " tracks)";
                            var groupTrackListScrollable = this.createScrollable(dxGroupParamValue, "dxScrollList");
                            thisDxParamItem.appendChild(groupTrackListScrollable);
                        } else {
                            var groupOtherParam = this.dx.createTextDiv(dxGroupParamValue);
                            thisDxParamItem.appendChild(groupOtherParam);
                        }
                        thisDxGroupParamList.appendChild(thisDxParamItem);
                    }
                }
                thisDxGroupConfigParams.appendChild(thisDxGroupParamList);
                thisDxGroupDiv.appendChild(thisDxGroupConfigParams);
                return thisDxGroupDiv;

            },

            validateNewConfig: function() {
                // groupRange
                var newRange = parseInt(registry.byId('p_range_' + this.groupId).get("value"), 10);
                if (newRange > 100) newRange = 100;
                if (newRange < 0) newRange = 0;
                if (newRange !== this.groupRange) this.set("groupRange", newRange);
                // groupMaxTracks
                var newMaxTracks = parseInt(registry.byId('p_maxTracks_' + this.groupId).get("value"), 10);
                if (0 < newMaxTracks < 200 && newMaxTracks != this.groupMaxTracks) {
                    this.set("groupMaxTracks", newMaxTracks);
                }
                // groupMode
                var newMode = registry.byId('p_mode_' + this.groupId).get("value");
                if (newMode !== this.groupMode){
                    this.set("groupMode", newMode);
                    this.widget.set("widgetGroupMode", newMode);
                }
                // toggleFeatures
                var newShowFeaturesValue = registry.byId('p_filterfeat_' + this.groupId).get("value");
                if (newShowFeaturesValue === 'true') newShowFeatures = true;
                else newShowFeatures = false;
                if (newShowFeatures !== this.toggleFeatures) {
                    // change the button "checked" value without firing the event
                    this.widget.toggleFButton.set("checked", newShowFeatures);
                    // call the on change event from the widget that'll take care of changing values
                    this.widget.toggleFButton.onChange(newShowFeatures);
                }
                // toggleSignal
                var newShowSignalValue = registry.byId('p_filtersignal_' + this.groupId).get("value");
                if (newShowSignalValue === 'true') newShowSignal = true;
                else newShowSignal = false;
                if (newShowSignal !== this.toggleSignal) {
                    // change the button "checked" value without firing the event
                    this.widget.toggleSButton.set("checked", newShowSignal);
                    // call the on change event from the widget that'll take care of changing values
                    this.widget.toggleSButton.onChange(newShowSignal);
                }
            },

            /* Functions building elements*/
            createSelectDropdown: function(id, val) {
                var mySelect = new Select({
                    id: id,
                    options: [{
                        label: " Automatic ",
                        value: "automatic"
                    }, {
                        label: " Manual ",
                        value: "manual"
                    }, {
                        label: " Disabled ",
                        value: "disabled"
                    }]
                });
                mySelect.set('value', val);
                return mySelect;
            },

            createSelectFilterDropdown: function(id, val) {
                if (val === false || val === true){
                    if (val === false) val = "false";
                    if (val === true) val = "true";
                }
                var mySelect = new Select({
                    id: id,
                    options: [{
                        label: " True ",
                        value: "true"
                    }, {
                        label: " False ",
                        value: "false"
                    }]
                });
                mySelect.set('value', val);
                return mySelect;
            },

            createScrollable: function(pList, scrollSuppClass) {
                var textDiv = domConstruct.create("div", {
                    class: "scrollableList"
                });
                var counter = 0;
                array.forEach(pList, function(tr) {
                    counter++;
                    var trA = domConstruct.create('a', {
                        innerHTML: tr
                    });
                    if (counter % 2 === 0) {
                        domClass.add(trA, 'scrollItem1');
                    } else {
                        domClass.add(trA, 'scrollItem2');
                    }
                    textDiv.appendChild(trA);
                });
                if (!!scrollSuppClass) domClass.add(textDiv, scrollSuppClass);
                return textDiv;
            }
        });
    });
