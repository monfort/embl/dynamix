define(
    [
        "dojo/_base/declare",
        "dojo/_base/fx",
        "dojo/_base/lang",
        "dojo/_base/array",

        "dojo/dom-style",
        "dojo/dom-attr",
        "dojo/dom-class",

        "dojo/dom",
        "dojo/on",
        "dojo/Deferred",
        "dojo/mouse",
        "dijit/form/Button",
        "dijit/Tooltip",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "dijit/_HasDropDown",
        "dojo/text!./templates/masterWidgetTemplate.html",
        "dojo/domReady!"
    ],
    function(
        declare,
        baseFx,
        lang,
        array,
        domStyle,
        domAttr,
        domClass,
        dom,
        on,
        Deferred,
        mouse,
        Button,
        Tooltip,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        _HasDropDown,
        masterWidgetTemplate
    ){

        /* Declare a custom dropDown container here, used to store group widgets
        ** Basically, it is just a div where we can gather them, and used to construct a
        ** dropDown with the mainViewElementWidget that is extending _HasDropDown */
        declare("dxDropDown", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
            templateString: "<div class=\"masterWidgetDropDownMenu\" data-dojo-attach-point=\"dropDownMenu\"></div>",
            postCreate: function(){
                this.inherited(arguments);
            },
            _addGroupToDropDownMenu: function(groupWidget){
                groupWidget.placeAt(this.dropDownMenu, "last");
            }
        });

    return declare ("masterWidget", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _HasDropDown], {
        templateString: masterWidgetTemplate,

        dx:null,
        id: null,
        pluginName: null,    // (*)

        groupWidgets: null,
        groupWidgetsById: {},
        groupWidgetsList: [],
        groupWidgetsCount: null,

        groupWidgetsNames:[],

        totalShownNumber: null,
        totalRelevantNumber: null,
        totalRelevantShownNumber: null,

        /* User interactive elements*/
        masterAddButton: null,
        masterRefreshButton: null,
        masterRemoveButton: null,
        // list of corresponding buttons from group Widgets
        refreshButtonsList: [],
        addButtonsList: [],
        removeButtonsList: [],


        isMaxTracksReachedForAtLeastAGroup: null,
        allGroupsAllShown:null,
        dropDown: null,
        // CIRCLE STATE ICON
        stateInfoCircleDom: "",
        // COLORS OF THE CIRCLE STATE ICON
        greenStateInfoIconDom: "#00B624",
        orangeStateInfoIconDom : "#E38F00",
        redTextGlobalMTReached : "#FF0000",
        // ANIMATION
        stateColorCircleAnim: null,
        mouseAnim: null,

        constructor: function(args) {
            dojo.safeMixin(this,args);
            this.inherited(arguments);
            this.groupWidgets = {};
            this.groupWidgetsById = {};
            this.groupWidgetsNames = [];
            this.refreshButtonsList = [];
            this.addButtonsList = [];
            this.removeButtonsList = [];
            this.isMaxTracksReachedForAtLeastAGroup = false;
            this.allShownAllGroups = false;
        },

        _setTotalShownNumberAttr: function(tShow){
            this._set("totalShownNumber", tShow);
            this._updateTotalShownTrackNumber(tShow);
        },
        _setTotalRelevantNumberAttr: function(tRel){
            this._set("totalRelevantNumber", tRel);
            this._updateTotalRelevantTrackNumber(tRel);
        },
        _setTotalRelevantShownNumberAttr: function(tSRel){
            this._set("totalRelevantShownNumber", tSRel);
            this._updateTotalRelevantShownTrackNumber(tSRel);
        },

        _updateTotalShownTrackNumber: function(numShown){
            domAttr.set(this.totalShown, "innerHTML", numShown);
        },

        _updateTotalRelevantTrackNumber: function(numRelevant){
            domAttr.set(this.totalRelevant, "innerHTML", numRelevant);
        },

        _updateTotalRelevantShownTrackNumber: function(numShownRelevant){
            domAttr.set(this.totalRelevantShown, "innerHTML", numShownRelevant);
        },

        _setAllGroupsAllShownAttr: function(allS){
            this._set("allGroupsAllShown", allS);
        },

        /* DOJO BUG FIX
        ** see http://dojo-toolkit.33424.n3.nabble.com/Making-a-minimal-menu-dropdown-td1780079.html
        ** Avoid a scope error when closing the drop down menu */
        focus: function(){
            return this.focusNode;
        },

        // native dropDown button function, can overwrite because this element _HasDropDown
        _onDropDownMouseDown: function(ev){
            if (domClass.contains(ev.target, "dijitButtonNode")
                | domClass.contains(ev.target, "dijitButtonContents")
                | domClass.contains(ev.target, "fa")){
                ev.preventDefault();
            }
            else{
                this.toggleDropDown();
            }
        },

        postCreate: function(){
            this.inherited(arguments);
            //Construct dropDown
            this.dropDown = new dxDropDown();
            /* this.own gather event handlers / watchers. Allow easy removal of all of them,
            ** for example when widget.destroy() is called */
            this.own(
                this.dropDown,
                this.groupWidgets,
                // this own of the main widget should follow EVERY widget
                on(this.masterRefreshButton, "click", lang.hitch(this, function(evt){
                    this._masterButtonClickHandler(this.refreshButtonsList);
                })),
                on(this.masterAddButton, "click", lang.hitch(this, function(evt){
                    this._masterButtonClickHandler(this.addButtonsList);
                })),
                on(this.masterRemoveButton, "click", lang.hitch(this, function(evt){
                    this._masterButtonClickHandler(this.removeButtonsList);
                })),
                this.watch("groupWidgetsCount", lang.hitch(this, function(name, oldCount, newCount){
                    var innerString = newCount+" group";
                    if (newCount >1) innerString += "s";
                    domAttr.set(this["groupInfo_"+this.id], "innerHTML", innerString);
                })),
                this.watch("isMaxTracksReachedForAtLeastAGroup", lang.hitch(this, function(name, oldMaxTr, newMaxTr){
                    this._updateMasterMaxTracksWarningIcon(oldMaxTr, newMaxTr);
                })),
                this.watch("allGroupsAllShown", lang.hitch(this, function(name, oldAllGrpAllShwn, newAllGrpAllShwn){
                    this._updateWidgetStateInfoIcon(newAllGrpAllShwn);
                    this._checkMissingRelevantBlinking(newAllGrpAllShwn);
                }))
            );
        },


        /* Simulating single groupWidget click, one after the other
        **  That does not assume anything about the group mode
        ** If the group is disabled the onClick handler returns a dummy
        ** resolved promise to directly go to next
        **
        ** buttonList: the list of buttons contains either all the "refresh" button,
        ** either all  the "add" buttons or all the "remove" buttons. */
        _masterButtonClickHandler: function(buttonList){
            var clickHandlerPromise = this.dx.newResolvedDummyPromise();
            array.forEach(buttonList, lang.hitch(this, function(buttonToClick){
                clickHandlerPromise = clickHandlerPromise.then(lang.hitch(this, function(){
                    return buttonToClick.onClick();
                }));
            }));
        },

        _updateMasterMaxTracksWarningIcon: function(oldMaxTrReached, newMaxTrReached){
            // this.warningIcon
            // this.mainStateImage.src
            // if (!oldMaxTrReached && !!newMaxTrReached) domStyle.set(this.mainGroupMaxTrackIcon, 'display', 'inline-block');
            // if (!!oldMaxTrReached && !newMaxTrReached) domStyle.set(this.mainGroupMaxTrackIcon, 'display', 'none');
            // if (!!newMaxTrReached && ( this.totalRelevantShownNumber < this.totalRelevantNumber ) ) domStyle.set(this.mainGroupMaxTrackIcon, 'display', 'inline-block');
            if (!!newMaxTrReached) domStyle.set(this.mainGroupMaxTrackIcon, 'display', 'inline-block');
            if (!newMaxTrReached) domStyle.set(this.mainGroupMaxTrackIcon, 'display', 'none');
        },


        _updateWidgetStateInfoIcon: function(newAllGrpAllShwn){
            var stateInfoCircleColor = (newAllGrpAllShwn) ? this.greenStateInfoIconDom : this.orangeStateInfoIconDom;
            domStyle.set(this.stateMasterInfoCircle, "color", stateInfoCircleColor);
        },

        _checkMissingRelevantBlinking: function(newAllGrpAllShwn){
            if (!newAllGrpAllShwn){
                this._blinkStateColorCircle();
            }
            else if (newAllGrpAllShwn && this.stateColorCircleAnim) {
                this._cancelBlinkStateColorCircleAnim(this.stateColorCircleAnim);
            }
        },

        addDxGroupWidget: function(dxGroupWidget){

            var widgetId = dxGroupWidget.gid;
            this.groupWidgetsById[widgetId] = dxGroupWidget;
            this.groupWidgetsList.push(dxGroupWidget);
            this.groupWidgetsNames.push(widgetId)

            this.set("groupWidgetsCount", this.groupWidgetsNames.length);

            this.refreshButtonsList.push(dxGroupWidget.refreshButton);
            this.addButtonsList.push(dxGroupWidget.addButton);
            this.removeButtonsList.push(dxGroupWidget.removeButton);

            this.totalDxMaxTracksWatcher = this.enableTotalDxMaxTracksWatcher();
            domStyle.set(this.totalShown, "color", "black");
            domStyle.set(this.totalShown, "font-weight", "normal");
            this.own(
                dxGroupWidget,
                this.totalDxMaxTracksWatcher,
                dxGroupWidget.watch("groupMode", lang.hitch(this, function(name, oldMode, newMode){
                    this._onGroupModeChange(oldMode, newMode);
                })),
                dxGroupWidget.watch("isMaxTracksReached", lang.hitch(this, function(name, oldRchd, newRchd){
                    if (newRchd === true) this.set("isMaxTracksReachedForAtLeastAGroup", true);
                    else if (newRchd === false){
                        var groupsCount =  this.get("groupWidgetsCount");
                        var isMaxTracksReachedForAtLeastAGroup = this._checkMasterGroupMaxTracks();
                        this.set("isMaxTracksReachedForAtLeastAGroup", isMaxTracksReachedForAtLeastAGroup);
                    }
                })),
                dxGroupWidget.watch("groupAllShown", lang.hitch(this, function(name, oldShwn, newShwn){
                    if (newShwn === true) this.set("allGroupsAllShown", this._checkAllGroupsAllShown());
                    else if (newShwn === false) this.set("allGroupsAllShown", false);
                })),
                // watch numberShown
                dxGroupWidget.watch("numberShownOnInterval", lang.hitch(this, function(name, oldVal, newVal){
                    this._onNumberOnIntervalChange("numberShownOnInterval", "totalShownNumber");
                })),
                // watch numberRelevant
                dxGroupWidget.watch("numberRelevantOnInterval", lang.hitch(this, function(name, oldVal, newVal){
                    this._onNumberOnIntervalChange("numberRelevantOnInterval", "totalRelevantNumber");
                })),
                // watch numberRelevantShown
                dxGroupWidget.watch("numberRelevantShownOnInterval", lang.hitch(this, function(name, oldVal, newVal){
                    this._onNumberOnIntervalChange("numberRelevantShownOnInterval", "totalRelevantShownNumber");
                }))
            );
            this._checkMasterButtons();
            this._onNumberOnIntervalChange("numberShownOnInterval", "totalShownNumber");
            this._onNumberOnIntervalChange("numberRelevantOnInterval", "totalRelevantNumber");
            this._onNumberOnIntervalChange("numberRelevantShownOnInterval", "totalRelevantShownNumber");
            this.set("isMaxTracksReachedForAtLeastAGroup", this._checkMasterGroupMaxTracks());
            this.set("allGroupsAllShown", this._checkAllGroupsAllShown());
            this.dropDown._addGroupToDropDownMenu(dxGroupWidget);
        },


        enableTotalDxMaxTracksWatcher: function() {
            return this.watch("totalShownNumber", lang.hitch(this, function(name, oldMt, newMt) {
                if (newMt >= this.dx.totalDxMaxTracks){
                    domStyle.set(this.totalShown, "color", this.redTextGlobalMTReached);
                    domStyle.set(this.totalShown, "font-weight", "bold");
                }
                else if (newMt < this.dx.totalDxMaxTracks){
                    domStyle.set(this.totalShown, "color", "black");
                    domStyle.set(this.totalShown, "font-weight", "normal");
                }
            }));
        },

        _onGroupModeChange: function(oldMode, newMode){
            this._checkAllGroupsAllShown();
            this._checkMasterButtons();
        },

        _checkAllGroupsAllShown: function(){
            // For all the other cases we have to check!
            var allGroupsAllShown = true;
            for (i=0; i<this.get("groupWidgetsCount"); i++){
                var widgetId = this.groupWidgetsNames[i];
                if (this.groupWidgetsById[widgetId].get("widgetGroupMode") !== "disabled"
                    && this.groupWidgetsById[widgetId].get("groupAllShown") === false){
                    allGroupsAllShown = false;
                    break;
                }
            }
            return allGroupsAllShown;
        },

        _checkMasterButtons: function(){
            if (array.every(this.groupWidgetsList, lang.hitch(this, function(groupWidget){
                return (groupWidget.get("widgetGroupMode") === "disabled");
            }))){
                this.masterAddButton.setDisabled(true);
                this.masterRefreshButton.setDisabled(true);
                this.masterRemoveButton.setDisabled(true);
            } else {
                this.masterAddButton.setDisabled(false);
                this.masterRefreshButton.setDisabled(false);
                this.masterRemoveButton.setDisabled(false);
            }
        },

        /*
        */
        _checkMasterGroupMaxTracks: function(){
            var isMaxTracksReachedForAtLeastAGroup = false;
            for (i=0; i<this.get("groupWidgetsCount"); i++){
                var widgetId = this.groupWidgetsNames[i];
                if (this.groupWidgetsById[widgetId].get("isMaxTracksReached") === true){
                    isMaxTracksReachedForAtLeastAGroup = true;
                    break;
                }
            }
            return isMaxTracksReachedForAtLeastAGroup;
        },

        _onNumberOnIntervalChange:function(numberOnIntervalStrId, totalStrId){
                var totalNumber = this._countWidgetTotal(numberOnIntervalStrId);
                this.set(totalStrId, totalNumber);
        },

        _countWidgetTotal: function(countedAttrName){
            var totalCount = 0;
            for (i=0; i<this.get("groupWidgetsCount"); i++){
                var widgetId = this.groupWidgetsNames[i];
                var countedAttrValue = this.groupWidgetsById[widgetId].get(countedAttrName);
                totalCount = totalCount + countedAttrValue;
            }
            return totalCount;
        },

        _blinkStateColorCircle: function(){
            if (this.stateColorCircleAnim){
                this._cancelBlinkStateColorCircleAnim(this.stateColorCircleAnim);
            }
            var stateColorCircleNode = this.stateMasterInfoCircle;
            this.stateColorCircleAnim = baseFx.animateProperty({
                node: stateColorCircleNode,
                duration: 650,
                properties: {
                    opacity: { start: '1', end:  '0.45' }
                },
                repeat: 20
            }).play();
        },

        _cancelBlinkStateColorCircleAnim: function(anim){
            anim.stop(true);
            anim = null;
            domStyle.set(dom.byId(this.stateMasterInfoCircle), "opacity", "1");
        }
    });
});
