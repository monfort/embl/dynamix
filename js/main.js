define(
    [
    'dijit/Dialog',
    'dijit/form/Button',
    'dijit/MenuItem',
    'dijit/registry',

    'dojo/_base/array',
    'dojo/_base/declare',
    'dojo/_base/json',
    'dojo/_base/lang',
    'dojo/aspect',
    'dojo/cookie',
    'dojo/Deferred',
    'dojo/dom',
    'dojo/dom-attr',
    'dojo/dom-class',
    "dojo/dom-style",
    'dojo/dom-construct',
    'dojo/on',
    'dojo/query',
    'dojo/ready',
    'dojo/topic',
    "dojo/promise/all",
    'dojo/when',
    "dojo/Stateful",

    'dijit/form/TextBox',
    "dijit/form/NumberTextBox",

    './dxTrackGroup',
    './widgets/masterWidget',
    'JBrowse/GenomeView',
    'JBrowse/Plugin',
    'JBrowse/Util',
    "dojo/domReady!"
],
    function(
    Dialog,
    Button,
    dMenuItem,
    registry,

    array,
    declare,
    JSON,
    lang,
    aspect,
    cookie,
    Deferred,
    dom,
    domAttr,
    domClass,
    domStyle,
    domConstruct,
    on,
    query,
    ready,
    topic,
    pall,
    when,
    Stateful,

    TextBox,
    NumTextBox,

    dxTrackGroup,
    masterWidget,
    GenomeView,
    JBrowsePlugin,
    Util
) {
return declare([JBrowsePlugin, Stateful],
{
    isEnabled: false,
    navigationListener: null,
    masterWidgetDivId: "masterWidgetDiv",
    masterWidgetId: "mainWidget",
    dxMasterWidget: null,
    dynamixConfig: {},
    dxTrackGroups: {},
    dxGroupIdArray: [],
    pluginMacroTrackInfo: {},
    pluginGroupWidgets: {},
    pluginMainWidget: null,
    hidePromisesCollection: {},
    totalDxMaxTracks: null,
    totalDxMaxTracksWatcher:null,
    movementSensitivityThreshold: 10, // %
    bgCol : {},
    borderCol:{
        // SOME COLORS
        // http://www.hitmill.com/html/pastels2.html
        "green" : "green",
        "red" : "red",
        "blue" : "blue",
        "yellow" : "yellow",
        "purple" : "purple",
        "orange" : "orange",
        "black" : "black",
        // GREEN
        "darkergreen": "#eaf5e1",
        "lightgreen" : "#F1F8EB",
        "lightergreen" : "#fbfdfa",
        // GRAY
        "darkergray" : "#CCCCCC",
        "lightgray" : "#CACACA",
        "lightergray" : "#F0F0F0",
        // ORANGE
        "darkerorange" : "#FFCFA6",
        "lightorange" : "#FFE3CD",
        "lighterorange" : "#FFF8F2",
        // BLUE
        "darkerblue" : "#d1f0f9",
        "lightblue" : "#e3f6fb",
        "lighterblue":"#f5fcfd"
    },
    // generic __________________________________________________________________________
    isNumber: function(n) {
        return (Object.prototype.toString.call(n) === '[object Number]'
                    || Object.prototype.toString.call(n) === '[object String]')
                    && !isNaN(parseFloat(n))
                    && isFinite(n.toString().replace(/^-/, ''));
    },

    constructor: function(){
        var thisP = this;
        var thisBrowser = this.browser;
        //Load dynamix plugin CSS files
        this.loadCssFiles(); // loaded once, /!\ hardcoded filenames
        this.dynamixConfig = this.browser.config.dynamix || {};
        this.constructDynamixMenuElements();
        var dxSettings = registry.byId("menubar_dxSettings");
        var dxActivator = registry.byId("menubar_dxActivator");
        thisBrowser.afterMilestone('completely initialized', function(){
            var jbrowseMenu = query(".menuBar")[0];
            /* Menu elements */
            /* Add item to menu then menu to menuBar */
            thisBrowser.addGlobalMenuItem( 'plugin', dxActivator );
            thisBrowser.addGlobalMenuItem( 'plugin', dxSettings);
            thisBrowser.addGlobalMenuItem( 'plugin', registry.byId("menubar_dxHelp") );
            /* Do not add the settings menu element if no config has been found
            ** Instead alert error */
            if (thisP.dynamixConfig != {}){
                dxActivator.set("disabled", false); // enable activation button
                dxSettings.set("disabled", false); // enable parametor button
            }
            else{
                alert("It appears that you installed Dynamix but did not create any configuration. Create it and relaod the page");
            }
            thisBrowser.renderGlobalMenu('plugin', {text: 'Dynamix'}, jbrowseMenu);
        });
    },

    loadCssFiles: function(){
        /*/!\ hardcoded CSS filenames /!\*/
        var fontAwesome  = require.toUrl("plugins/"+this.name+"/icons/css/font-awesome.css");
        domConstruct.create("link", { href:fontAwesome, rel:'stylesheet' }, document.getElementsByTagName('head')[0]);
        var mainCss  = require.toUrl("plugins/"+this.name+"/css/main.css");
        domConstruct.create("link", { href:mainCss, type:'text/css', rel:'stylesheet' }, document.getElementsByTagName('head')[0]);
        var mainWidgetCss  = require.toUrl("plugins/"+this.name+"/js/widgets/css/masterWidget.css");
        domConstruct.create("link", { href:mainWidgetCss, type:'text/css', rel:'stylesheet' }, document.getElementsByTagName('head')[0]);
        var groupWidgetCss  = require.toUrl("plugins/"+this.name+"/js/widgets/css/dxTrackGroupWidget.css");
        tstcss = domConstruct.create("link", { href: groupWidgetCss, type:'text/css', rel:'stylesheet' }, document.getElementsByTagName('head')[0]);
    },

    /* *****************************
    ** UI CONSTRUCTION **
    *******************************/

    /*
    ** Menu's items creation
    **
    */
    constructDynamixMenuElements: function(){
        var thisP = this;
        /* CONFIG DIALOG MENU ENTRY */
        var dxSettings = new dMenuItem({
            id: 'menubar_dxSettings',
            label: 'Settings',
            disabled: true,
            onClick: function(){
                /* Construction occurs on each click to get an up to date version.
                ** On each "cancel" click, it's destroyed. */
                var menuConfigDialog = thisP.constructConfigDialog();
                menuConfigDialog.show();
            }
        });
        /* ACTIVATION MENU ENTRY */
        var dxActivator = new dMenuItem({
            id: 'menubar_dxActivator',
            label: 'Enable Dynamix',
            disabled: true,
            onClick: function(){
                if (!thisP.isEnabled) {
                    thisP.activatePlugin(false, false);
                }
                else if (thisP.isEnabled){
                    domAttr.set(this, 'label', "Enable Dynamix");
                    thisP.deactivatePlugin();
                }
            }
        });
        var dxHelpDialog = this.constructHelpDialog();
        /* HELP MENU ENTRY */
        var dxHelp = new dMenuItem({
            id: 'menubar_dxHelp',
            label: 'Help',
            onClick: function(){
                dxHelpDialog.show();
            }
        });
    },


    /***********************************************
    ** configuration's dialog constructor **
    ***********************************************/

    constructConfigDialog: function (){
        var thisP = this;

        var groupsConfigurations = this.dynamixConfig['group_configurations'] || {};
        var orderedGroupStackIds = this.getGroupStackOrder(groupsConfigurations);

        dialogButton = function(id, innerHtml){
            var newButt = new Button({
                id: id,
                innerHTML: innerHtml
            });
            return newButt;
        };

        var myDialogContentDiv = domConstruct.create("div", { class: "dxDialogContent" });

        var myConfigDialog = new Dialog({
            id: 'dynamixConfigDialog',
            title: "Dynamix Configuration",
            content: myDialogContentDiv,
            class: "dxDialog",
            style: "width: 650px"
        });

        on(myConfigDialog, "cancel", function(){
            //clicking on the window's top right cross
            this.hide();
            this.destroyRecursive();
        });

        var groupContentDiv = domConstruct.create("div", { class: "dxDialogGroupContent" });
        var dialogButtonDiv = domConstruct.create("div", {class: 'dxButtons'});
        var validButton = dialogButton("configValidButton", "OK");
        var cancelButton = dialogButton("configCancelButton", "Cancel");
        dialogButtonDiv.appendChild(validButton.domNode);
        dialogButtonDiv.appendChild(cancelButton.domNode);

        on(cancelButton, "click", function(){
            myConfigDialog.hide();
            myConfigDialog.destroyRecursive();
        });

        on(validButton, "click", lang.hitch(this, function(){
            var elem = null;
            var newValue = null;
            var newGlobalMaxTracks = parseInt(registry.byId('p_globmaxtracks').get("value"), 10);
            if (newGlobalMaxTracks !== this.get("totalDxMaxTracks")) this.set("totalDxMaxTracks", newGlobalMaxTracks);
            var validConfProm = this.newResolvedDummyPromise();
            var allValidateNewConfig = [validConfProm];

            array.forEach(orderedGroupStackIds, lang.hitch(this, function(groupId){
                validConfProm = validConfProm.then(lang.hitch(this, function(){
                    if (groupsConfigurations.hasOwnProperty(groupId)){
                        var validConfProm = this.dxTrackGroups[groupId].validateNewConfig();
                    }
                    else{
                        validConfProm = this.newResolvedDummyPromise();
                    }
                    allValidateNewConfig.push(validConfProm);
                }));
            }));
            myConfigDialog.hide();
            pall(allValidateNewConfig).then(lang.hitch(this, function(){
                myConfigDialog.destroyRecursive();
                this.afterMove(this.dxGroupIdArray, this.browser.view.visibleTrackNames()).then(lang.hitch(this, function(visibleTrackLabels){
                    this.visibleTrackLabels = visibleTrackLabels;
                }));
            }));
        }));
        myDialogContentDiv.appendChild(dialogButtonDiv);

        var dxGlobConfigParams = domConstruct.create("div", {
            id:"globalConfParam",
            class: "dxGroupConfParams"
        });
        var dxParamList = domConstruct.create("ul");
        var dxParamItem = domConstruct.create("li");
        var globalMaxTrackTitle = this.createTextDiv("Global Max Tracks", "dxConfParamName");
        globalMaxTrackTitle.innerHTML = globalMaxTrackTitle.innerHTML;
        dxParamItem.appendChild(globalMaxTrackTitle);
        var totalDxMaxTracks = this.get("totalDxMaxTracks");
        var rangeTextbox = this.createNumberTextBox('p_globmaxtracks', totalDxMaxTracks, 1000, "dxRangeTextBox");

        dxParamItem.appendChild(rangeTextbox.domNode);
        dxParamList.appendChild(dxParamItem);
        dxGlobConfigParams.appendChild(dxParamList);
        myDialogContentDiv.appendChild(dxGlobConfigParams);

        myDialogContentDiv.appendChild(groupContentDiv);

        myDialogContentDiv.appendChild(groupContentDiv);
        array.forEach(orderedGroupStackIds, lang.hitch(this, function(groupId){
            if (groupsConfigurations.hasOwnProperty(groupId)){
                var thisDxGroupDiv = this.dxTrackGroups[groupId].constructGroupGonfigDom();
                groupContentDiv.appendChild(thisDxGroupDiv);
            }
        }));
        return myConfigDialog;
    },

    createTextDiv: function(divText, divSuppClass) {
        var textDiv = domConstruct.create("div", {
            class: "dxNormTextDiv",
            innerHTML: divText
        });
        if (!!divSuppClass) domClass.add(textDiv, divSuppClass);
        return textDiv;
    },

    createTextBox: function(id, val, txtBoxSuppClass) {
        var textBox = new TextBox({
            id: id,
            value: val,
            placeHolder: val
        });
        if (!!txtBoxSuppClass) domClass.add(textBox, txtBoxSuppClass);
        return textBox;
    },

    createNumberTextBox: function(id, val, maxVal, txtBoxSuppClass) {
        var textBox = new NumTextBox({
            id: id,
            value: val,
            placeHolder: val,
            constraints: {
                min: 0,
                max: maxVal
            },
            invalidMessage: "Enter a valid integer value.",
            rangeMessage: "Enter a value between 0 and " + maxVal + "."
        });
        if (!!txtBoxSuppClass) domClass.add(textBox, txtBoxSuppClass);
        return textBox;
    },

    _totalDxMaxTracksGetter: function() {
        return this.totalDxMaxTracks;
    },
    _totalDxMaxTracksSetter: function(tmt) {
        this.totalDxMaxTracks = tmt;
    },

    enableTotalDxMaxTracksWatcher: function() {
        return this.watch("totalDxMaxTracks", lang.hitch(this, function(name, oldMt, newMt) {
            this.dxCookie("globalDxMaxTracks", newMt);
            if (this.dxMasterWidget.get("totalShownNumber") >= newMt){
                var numberOfTracksToRemove = this.dxMasterWidget.get("totalShownNumber") - newMt;
                var allVisibleDxLabels = this.visibleDxLabels();
                var visibleDxLabelsToHide = allVisibleDxLabels.slice(-numberOfTracksToRemove);
                this.viewHideTracks(visibleDxLabelsToHide);
            }
            else if (this.dxMasterWidget.get("totalShownNumber") < newMt){
                this.afterMove(this.dxGroupIdArray, this.browser.view.visibleTrackNames()).then(lang.hitch(this, function(visibleTrackLabels){
                    this.visibleTrackLabels = visibleTrackLabels;
                }));
            }
        }));
    },


    /* help dialog constructor */

    constructHelpDialog: function(){
        var myHelpInnerHtml = 'Welcome on Dynamix\'s help dialog.<br/><br/>Access Dynamix description page at ';
        var myHelpInnerHtml2 = '.<br/>You can ask for support on our google group webpage (';
	var myHelpInnerHtml3 = ')<br/>or directly send an email to '
	var myHelpInnerHtml4 = '.<br/><br>Matthias Monfort, 2017<br/><br/>'

        var wikiLinks = domConstruct.create("a", {
            class: 'help_wikilink',
            href: "http://furlonglab.embl.de/dynamix",
            target: "_blank",
            innerHTML:"furlonglab.embl.de/dynamix"
        });
        var ggLink = domConstruct.create("a", {
            class: 'help_wikilink',
            href: "https://groups.google.com/forum/#!forum/dynamix-support",
            target: "_blank",
            innerHTML:"https://groups.google.com/forum/#!forum/dynamix-support"
        });
        var ggMail = domConstruct.create("a", {
            class: 'help_wikilink',
            href: "mailto:dynamix-support@googlegroups.com",
            target: "_blank",
            innerHTML:"dynamix-support@googlegroups.com"
        });

        var validButton = new Button({
            id: 'help_button',
            innerHTML: "OK",
            class: 'helpButton'
        });

        var myHelpContent = domConstruct.create("div", {
		class: 'helpDialogContent',
		style: "font-size: 16px; text-align:center",
	});
        myHelpContent.innerHTML = myHelpInnerHtml;
        myHelpContent.appendChild(wikiLinks);
        myHelpContent.innerHTML = myHelpContent.innerHTML + myHelpInnerHtml2;
        myHelpContent.appendChild(ggLink);
        myHelpContent.innerHTML = myHelpContent.innerHTML + myHelpInnerHtml3;
        myHelpContent.appendChild(ggMail);
        myHelpContent.innerHTML = myHelpContent.innerHTML + myHelpInnerHtml4;

        myHelpContent.appendChild(validButton.domNode);

        var myHelpDialog = new Dialog({
            id: 'pluginHelpDialog',
            title: "Dynamix Help!",
            content: myHelpContent,
            class: "myPersonalHelpDialog"
        });

        on(myHelpDialog, "cancel", function(){
            this.hide();
        });
        on(validButton, "click", function(){
            myHelpDialog.hide();
        });
        return myHelpDialog;
    },

    postscript: function(){
        this.inherited(arguments);
        var thisP = this;
        /* predominant use of postscript() is to kick off the creation of groups */
        this.browser.afterMilestone('completely initialized', lang.hitch(this, function(){
            var jbrowseTracks = [];
            try{
                jbrowseTracks = this.getJBrowseAutoStartupTracks();
            }catch(noBroTrackErr){}
            var dxCookieActivation = this.dxCookie("state") === '1' ? true : false;
            var dxAutoActivation = this.dynamixConfig["default_state"] === 'active' ? true : false;
            /* Auto activate Dynamix if set so in cookies || config */
            if (dxCookieActivation === true || dxAutoActivation === true){
                this.waitUntilExpectedRenderingFinishes(jbrowseTracks, lang.hitch(this, function(){
                    this.afterExpectedRenderingFinished();
                }));
            }
        }));
    },

    /**********************************
    **  PLUGIN ACTIVATION **
    **********************************/

    getJBrowseAutoStartupTracks: function(){
        /* This is a snippet from JBrowse's Browser.js because Dynamix need this info */
        var tracksToShow = [];
        // always add alwaysOnTracks, regardless of any other track params
        if (this.browser.config.alwaysOnTracks) { tracksToShow = tracksToShow.concat(this.browser.config.alwaysOnTracks.split(",")); }
        // add tracks specified in URL track param,
        //    if no URL track param then add last viewed tracks via tracks cookie
        //    if no URL param and no tracks cookie, then use defaultTracks
        if (this.browser.config.forceTracks)   { tracksToShow = tracksToShow.concat(this.browser.config.forceTracks.split(",")); }
        else if (this.browser.cookie("tracks")) { tracksToShow = tracksToShow.concat(this.browser.cookie("tracks").split(",")); }
        else if (this.browser.config.defaultTracks) { tracksToShow = tracksToShow.concat(this.browser.config.defaultTracks.split(",")); }
        // currently, force "DNA" _only_ if no other guides as to what to show?
        //    or should this be changed to always force DNA to show?
        if (tracksToShow.length === 0) { tracksToShow.push("DNA"); }
        // eliminate track duplicates (may have specified in both alwaysOnTracks and defaultTracks)
        tracksToShow = Util.uniq(tracksToShow);
        var knownJBrowseLabels = Object.keys(this.browser.trackConfigsByName);
        return array.filter(tracksToShow, lang.hitch(this, function(t){
            if (array.indexOf(knownJBrowseLabels, t) !== -1) return t;
        }));
    },

    afterExpectedRenderingFinished: function(){
        return this.browser.afterMilestone('initView', lang.hitch(this, function() {
            this.browser.afterMilestone('completely initialized', lang.hitch(this, function() {
                this.activatePlugin();
            }));
        }));
    },

    waitUntilExpectedRenderingFinishes: function(jbrowseTracks, callback){
        var visibleTrackNames = this.browser.view.visibleTrackNames();
        // compare visible tracks with a list of parameters
        var missingTrackLabels = array.filter(jbrowseTracks, lang.hitch(this, function(lab){
            if (array.indexOf(visibleTrackNames, lab) === -1) return lab;
        }));
        if (missingTrackLabels.length > 0){
            lang.hitch(this, setTimeout(lang.hitch(this, this.waitUntilExpectedRenderingFinishes), 1000, jbrowseTracks, callback));
        } else {
            // Activate (callback is afterExpectedRenderingFinished)
            callback();
        }
    },

    getGroupStackOrder: function(allGroupsConfigurations){
        /* Recover the order in which the group will be displayed.
        ** Groups with a "stack_order" key in conf will be prioritise in the order defined by the value.
        ** Groups with a duplicated stack order value or none will be added at the end of the list. */
        var orderedStackIds = [];
        var nonOrderedStackIds = [];
        for (var groupId in allGroupsConfigurations){
            if (allGroupsConfigurations.hasOwnProperty(groupId)){
                groupConf = allGroupsConfigurations[groupId];
                if (groupConf.hasOwnProperty("stack_order")){
                    var stackOrderIndex = parseInt(groupConf["stack_order"], 10);
                    if (!orderedStackIds[stackOrderIndex]){
                        orderedStackIds[stackOrderIndex] = groupId;
                    } else{ nonOrderedStackIds.push(groupId); }
                } else{ nonOrderedStackIds.push(groupId); }
            }
        }
        return array.filter(orderedStackIds.concat(nonOrderedStackIds), function(gid){ if (gid) return gid; });
    },


    activatePlugin: function(){
        if (this.isEnabled === true) return;
        var thisView = this.browser.view;
        /* create main element , add group to it one by one */
        var masterWidgetDiv = registry.byId(this.masterWidgetDivId) || 'undefined';
        if (masterWidgetDiv === 'undefined'){
            masterWidgetDiv = this.createMasterDiv(this.masterWidgetDivId);
            domConstruct.place(masterWidgetDiv, dom.byId("navbox"), "last");
        }
        /* recover the groups configuration */
        allGroupsConfigurations = this.dynamixConfig['group_configurations'] || {};
        /* create the stack order checking the configuration*/
        orderedGroupStackIds = this.getGroupStackOrder(allGroupsConfigurations);
        if (orderedGroupStackIds.length > 0){
            var groupIdBorderColorMap = {}
            domAttr.set(masterWidgetDiv, "innerHTML", "");
            this.dxMasterWidget = new masterWidget(
                                                   {
                                                        "dx": this,
                                                        "id" : this.masterWidgetId,
                                                        "pluginName": this.name
                                                    });

            domConstruct.place(registry.byId(this.masterWidgetId).domNode, dom.byId(this.masterWidgetDivId), "replace");
            /* filter all tracks config to extract dynamix track config */
            dynamixTracksConfigs = array.filter(this.browser.config.tracks, function(t){
                if (t.hasOwnProperty("dynamix_id")) return t;
            });
            /* Here we iterate on groups in the given stack order */
            array.forEach(orderedGroupStackIds, lang.hitch(this, function(groupId){
                var groupConfiguration;
                if (allGroupsConfigurations.hasOwnProperty(groupId)){
                        groupConfiguration = allGroupsConfigurations[groupId];
                        var listOrderedGroupTrackIdsList = groupConfiguration['group_track_ids'].split(",") || [];
                        var groupTrackConfigs = array.filter(dynamixTracksConfigs, lang.hitch(this, function(dxt){
                            if (array.indexOf(listOrderedGroupTrackIdsList, dxt.dynamix_id) !== -1) return dxt;
                        }));
                        // Check STRING TOGGLE FEATURES / SIGNAL
                        // if (typeof(newTogSig) === "boolean" && newTogSig === true) newTogSig = "true";
                        // else if (typeof(newTogSig) === "boolean" && newTogSig === false) newTogSig = "false";
                        var groupColor = groupConfiguration['borderCol']

                        // NEW
                        groupIdBorderColorMap[groupId] = groupConfiguration['borderCol']
                        // END NEW

                        var dxTrackGp = new dxTrackGroup({
                            "dx" : this,
                            "groupId" : groupId,
                            "groupConfiguration" : groupConfiguration || {},
                            "groupTrackConfigs" : groupTrackConfigs || [],
                            "orderedGroupStackIds" : orderedGroupStackIds,
                            "listOrderedGroupTrackIdsList" : listOrderedGroupTrackIdsList,
                            "groupColor" : groupColor
                        });
                        this.dxTrackGroups[groupId] = dxTrackGp;
                        this.dxGroupIdArray.push(groupId);
                    }
            }));
            this.isEnabled = !this.isEnabled;
            if (this.isEnabled === true){
                /* INIT
                ** Enable the settings menu item */
                domAttr.set(registry.byId("menubar_dxActivator"), 'label', "Disable Dynamix");
                registry.byId("menubar_dxSettings").set("disabled", false); // enable parametor button
                this.totalDxMaxTracks = this.dynamixConfig['globalDxMaxTracks'] || this.dynamixConfig['global_max_tracks'] || 80;
                this.totalDxMaxTracksWatcher = this.enableTotalDxMaxTracksWatcher();
                this.dxCookie("state", 1);
            }
            this.genomicInterval = this.browser.view.visibleRegion();

            // Enable groups
            setTimeout(lang.hitch(this, function(){
                this.navigationListener = this.enableAroundMoveAspectHandler();
                this.afterMove(this.dxGroupIdArray, this.browser.view.visibleTrackNames()).then(lang.hitch(this, function(visibleTrackLabels){
                    this.visibleTrackLabels = visibleTrackLabels;
                }));
            }, 5000));
        }
        else{
            this.isEnabled = false;
            console.warn("No dynamix group found in configuration");
        }
        this.browser.browserWidget.resize();
    },

    createMasterDiv: function(masterWidgetDivId){
        return domConstruct.create("div",{
            id: masterWidgetDivId,
            class: "dijitReset dijitInline dijitLeft masterWidgetElementBase",
            innerHTML: "No group configuration found"
        });
    },

    deactivatePlugin: function(){
        /* Deactivate the plugin and destroy "Destroyable" elements. See dojo/destroyable */
        registry.byId("menubar_dxSettings").set("disabled", true); // enable parametor button
        this.totalDxMaxTracksWatcher.remove();
        this.removeDxCookie("state");
        this.navigationListener.remove();
        /* destroy masterWidgetDiv */
        registry.byId(this.masterWidgetId).destroyRecursive();
        domConstruct.destroy(dom.byId(this.masterWidgetDivId));
        /* For each group, disable and destroy group */
        array.forEach(this.dxGroupIdArray, lang.hitch(this, function(groupId){
            this.dxTrackGroups[groupId].widget.destroyRecursive();
            this.dxTrackGroups[groupId].disableGroup();
            this.dxTrackGroups[groupId].destroy();
            delete this.dxTrackGroups[groupId];
        }));
        this.dxGroupIdArray = [];
        this.dxTrackGroups = {};
        this.genomicInterval = {};
        delete this.dxTrackGroups;
        this.isEnabled = !this.isEnabled;
    },


    /*******************************
    ** BASIC FUNCTIONS **
    *******************************/

    getTrackStore: function(trackName){
        thisBrowser = this.browser;
        var trackStore = null;
        if (thisBrowser.trackConfigsByName.hasOwnProperty(trackName)){
            if (thisBrowser.trackConfigsByName[trackName].hasOwnProperty('store')){
                var trackStoreName = thisBrowser.trackConfigsByName[trackName].store;
                thisBrowser.getStore(trackStoreName, function(store){
                    trackStore = store;
                });
            }
            else{
                console.warn("Store of %s not found. Index the corresponding file with a trackLabel.", trackName);
            }
        }
        else{
            console.warn("Store of %s not found. Check the track label, it must be the same than"
                         + "the group_track_name in the plugin configuration @data/dynamix_conf.json", trackName);
        }
        return trackStore ;
    },

    asyncGetTrackStore: function(trackName){
        var getTrackStoreWithDeferred = new Deferred();
        thisBrowser = this.browser;
        var trackStore = null;
        var trackStoreName = null;
        try{
            trackStoreName = this.browser.trackConfigsByName[trackName].store;
        }catch(errTrackName){
            console.log("NO STORE FOR ", trackName, errTrackName);
            getTrackStoreWithDeferred.reject(errTrackName);
        }
        try{
            this.browser.getStore(trackStoreName, function(store){
                trackStore = store;
                getTrackStoreWithDeferred.resolve(trackStore);
            });
        }catch(noStoreErr){
            console.log("NO STORE FOR ", trackName, noStoreErr);
            getTrackStoreWithDeferred.reject(noStoreErr);
        }
        return getTrackStoreWithDeferred;
    },

    visibleDxLabels: function(){
        var visibleTrackNames = this.browser.view.visibleTrackNames();
        var visDxLabels = array.filter(visibleTrackNames, lang.hitch( this, function(tName){
            visibleTrack = this.browser.trackConfigsByName[tName] || {};
            if (visibleTrack.hasOwnProperty("dynamix_id")) return visibleTrack.label;
        }));
        return visDxLabels;
    },

    /***********************************************
    ** DYNAMIX MAIN FEATURE : AFTER MOVE BEHAVIOR **
    ************************************************/
    /* Make use of JBrowse native topic "NAVIGATE"
    ** ('/jbrowse/v1/n/navigate') for executing the
    ** dynamic behavior after user changed the
    ** observed genomic interval. */
    enableAroundMoveAspectHandler: function(){
        return this.browser.subscribe('/jbrowse/v1/n/navigate', lang.hitch(this, function(newRegionObject){
            var visibleTrackLabels;
            /* If chromosome has changed, need to recover the visible track list from a saved variable
            ** because calling JBrowse visibleTrackNames() function returns undefined */
            if (this.genomicInterval.ref !== newRegionObject.ref){
                visibleTrackLabels = this.visibleTrackLabels;
                this.waitUntilExpectedRenderingFinishes(visibleTrackLabels, lang.hitch(this, function(){
                    this.browser.publish('/dynamix/tracks/dx/colorise/', visibleTrackLabels);
                    this.genomicInterval = newRegionObject;
                    this.afterMove(this.dxGroupIdArray, visibleTrackLabels).then(lang.hitch(this, function(visLabel){
                        this.visibleTrackLabels = visLabel;
                    }));
                }));
            }
            else{
                visibleTrackLabels = this.browser.view.visibleTrackNames();
                this.genomicInterval = newRegionObject;
                this.afterMove(this.dxGroupIdArray, visibleTrackLabels).then(lang.hitch(this, function(visLabel){
                    this.visibleTrackLabels = visLabel;
                }));
            }
        }));
    },


    afterMove: function(dxGroupIdArray, visibleTrackLabels){
        return this.afterMoveComputeInfo(dxGroupIdArray).then(lang.hitch(this, function(){
            return this.afterMoveHideTracks(dxGroupIdArray, visibleTrackLabels).then(lang.hitch(this, function(vTrackLabels){
                return this.afterMoveShowTracks(dxGroupIdArray, vTrackLabels);
            }));
        }));
    },


    /* afterMoveComputeInfo retrieves the list of rlevant track IDs for each group
    ** that is the track IDs obtained from features overlapping the visible region IF the
    ** move is significant (see group module for computation of "significant-ness"). IF NOT
    ** then the information about relevant track is not updated
    ** ARGUMENTS:
    ** dxGroupIdArray the array of dynamix group IDs
    ** RETURNS:
    ** A  promise build by promise/all using a list of promise.
    ** Each them indicate that the group was updated
    */
    afterMoveComputeInfo:function(dxGroupIdArray){
        var computedInfoPromise;
        var arrayOfCompInfoPromises = [];
        array.forEach(dxGroupIdArray, lang.hitch(this, function(dxGroupId){
            var dxGroup = this.dxTrackGroups[dxGroupId];
            if (dxGroup.get("groupMode") !== "disabled"
                && dxGroup.isMoveSignificant(this.genomicInterval) === true)
            {
                // Remember the last genomic interval where the move has been considered significant
                dxGroup.moveIsSignificant(this.genomicInterval);
                computedInfoPromise = dxGroup.asyncGetTrackIdsFromFeatures(this.genomicInterval);
            }
            else{ computedInfoPromise =  this.newResolvedDummyPromise([]); } // a dummy promise
            arrayOfCompInfoPromises.push(computedInfoPromise);
        }));
        return pall(arrayOfCompInfoPromises);
    },

    /*
    */
    afterMoveHideTracks: function(dxGroupIdArray, visibleTrackLabels){
        var trackToHidePromise;
        var arrayOfGroupHiddenPromises = [];
        array.forEach(dxGroupIdArray, lang.hitch(this, function(dxGroupId){
            var groupTrackLabelsToHidePromise =  this.getGroupTrackLabelsToHide(dxGroupId, visibleTrackLabels);
            arrayOfGroupHiddenPromises.push(groupTrackLabelsToHidePromise);
        }));
        return pall(arrayOfGroupHiddenPromises).then( lang.hitch(this, function(allTracksToHide){
            var flattenAllTracksToHide = [].concat.apply([], allTracksToHide);
            return this.controllerHideTracks(flattenAllTracksToHide);
        }));
    },

    /*
    ** getGroupTrackLabelsToHide uses the group attribute "relevantTrackIds" to compute the relevant track labels list
    ** Returns a promise resolved when the list of track labels to hide has been computed
    ** If the group is disabled there is nothing to hide so returns a resolved promise with an empty list;
    */
    getGroupTrackLabelsToHide: function(dxGroupId, visibleTrackLabels){
        var dxGroup = this.dxTrackGroups[dxGroupId];
        if (dxGroup.get("groupMode") !== "disabled"){
            return when(dxGroup.getRelevantTrackLabelsFromRelevantTrackIds(), lang.hitch( this, function(relevantTrackLabels){
                var groupVisibleDxLabels = dxGroup.getShownOnIntervalLabelsList(visibleTrackLabels);
                return dxGroup.getTrackLabelsToHide(groupVisibleDxLabels, relevantTrackLabels);
            }));
        }
        else{
            return this.newResolvedDummyPromise([]);
        }
    },

    /*
    ** visibleTrackLabels ALL THE VISIBLE LABEL, NOT SPECIFIC TO A GROUP
    */
    afterMoveShowTracks: function(dxGroupIdArray, visibleTrackLabels){
        var groupVisibleTrackLabels;
        var groupShownPromise = this.newResolvedDummyPromise(visibleTrackLabels);
        var arrayOfGroupShownPromises = [groupShownPromise];
        array.forEach(dxGroupIdArray, lang.hitch(this, function(dxGroupId){
            groupShownPromise = groupShownPromise.then(lang.hitch(this, function(){
                /* All groups filter the same original list, it does not matter since one group updating
                ** is not gonna change the visible track list specific to another group
                ** that is, once filtered, the list contains only track from its own */
                groupVisibleTrackLabels = this.dxTrackGroups[dxGroupId].getShownOnIntervalLabelsList(visibleTrackLabels);
                /*NB: getShownOnIntervalLabelsList give the list in the order it is shown !!
                ** Not in the order of the group track IDs (see order param alpha/list) >>> This is for a good reason
                ** the reason is to be able to keep a consistent shown set of track when moving/looping*/
                var groupShownPromises = this.afterMoveTriggerGroupShow(dxGroupId, groupVisibleTrackLabels);
                arrayOfGroupShownPromises.push(groupShownPromises);
                return groupShownPromises;
            }));
        }));
        return groupShownPromise.then(lang.hitch(this, function(){
            return pall(arrayOfGroupShownPromises).then(lang.hitch(this, function(){
                return this.newResolvedDummyPromise(this.browser.view.visibleTrackNames());
            }));
        }));
    },


    /*
    */
    afterMoveTriggerGroupShow: function(groupId, groupVisibleTrackLabels){
        var dxGroup = this.dxTrackGroups[groupId];

        if (dxGroup.groupMode !== "disabled"){
            if (dxGroup.groupMode === "automatic"){
                // return dxGroup.asyncShowTracks();
                return dxGroup.betterSeqShowTracks(groupVisibleTrackLabels);
            }
            else{
                return this.newResolvedDummyPromise(groupVisibleTrackLabels);
            }
        }
        else{
            return this.newResolvedDummyPromise(groupVisibleTrackLabels);
        }
    },


    /*************************
    ** SHOW AND HIDE TRACKS **
    **************************/
    /* Hide tracks from a list of track labels. To achieve hiding tracks (i.e. to remove visible tracks
    ** from the track container) dynamix uses the native JBrowse way, that is publishing the
    ** information in a specific topic that will take care of the track removal.
    ** In the meantime, Dynamix needs to know when the tracks have really been removed, so that
    ** it can start adding other relevant tracks, so it creates a promise that is resolved when track have
    ** been removed. The hide promise is resolved by setting up an interval call to the function that
    ** attempts to resolve the said promise.
    ** The promise itself is returned by hide tracks to access the promise resolution result when needed. */

    hideTracks: function(trackToHideLabels, msgTopic){ /* array of strings: tracklabels*/
        // tracks are hidden by dynamix code, not by user
        var deferredHideTracks = new Deferred();
        // No need to try to resolve a promise if nothing was to hide
        if (trackToHideLabels.length === 0){
            this.resolveHidePromise(deferredHideTracks, msgTopic, [])
        }
        else{
            var toHideTracksInfo = this.getHideTrackConfigs(trackToHideLabels);
            this.browser.publish('/jbrowse/v1/c/tracks/hide', toHideTracksInfo['configs']); // ask JBrowse to hide tracks
            this.resolveHidePromise(deferredHideTracks, msgTopic, toHideTracksInfo['labels']) // check when JBrowse finished hiding tracks
        }
        return deferredHideTracks.promise;
    },

    /* From a list of labels, uses JBrowse functions to obtain the track configuration needed
    ** by JBrowse to hide tracks */
    getHideTrackConfigs: function(trackToHideLabels /*{Array|String} of tracks that got no signal */){
        if (typeof trackToHideLabels == 'string') trackToHideLabels = trackToHideLabels.split(',');
        /* Get the configurations of those tracks */
        var toHide_confs = [];
        var toHide_labels = [];
        array.forEach(trackToHideLabels, lang.hitch (this, function(track){
            /* Double check if those track are visible.
            ** Sometimes JBrowse does not filter it well on its own, which create duplicates */
            if (array.indexOf(this.browser.view.visibleTrackNames(), track) != -1){
                trackConfig = this.browser.trackConfigsByName[track];
                toHide_confs.push(trackConfig);
                toHide_labels.push(track);
            }
        }));
        return {
            "configs" : toHide_confs,
            "labels" : toHide_labels
        };
    },

    /* Once the track removal from the track container has been triggered, an interval
    ** call to the promise resolution function is set up (every 10ms seems fine) to check when
    ** JBrowse has fulfilled dynamix hiding request. */
    resolveHidePromise: function(hidePromise, msgTopic, hiddenTrackLabels){
        var resolveAttemptInterval = setInterval(lang.hitch(this, function(){
            this.attemptResolveHidePromise(hidePromise, msgTopic, hiddenTrackLabels, resolveAttemptInterval);
        }), 10); // ms
    },

    /* Check the list of visible track labels to see if does not contain the labels of the tracks that has been requested to
    ** hide. When every track is actually removed from the container, resolve the hide promise and clear the interval call
    ** The information that tracks have really been hidden is sent to dynamix to the appropriate topic (msgTopic). This
    ** topic differs depending on what triggered the removal of tracks. See the two functions below (controllerHideTracks
    ** & viewHideTracks) */
    attemptResolveHidePromise: function(hidePromise, msgTopic, hiddenTrackLabels, resolveAttemptInterval){
        var visibleDxLabels = this.browser.view.visibleTrackNames();
        if (array.every(hiddenTrackLabels, lang.hitch(this, function(dxLabel){
            return (array.indexOf(visibleDxLabels, dxLabel) === -1);
        })));
        {/* array.every is shorcicuited ASA one dxLabel has been found in the visibledxLabel
         ** Oppositely it returns true if all trackLabels have been hidden. The setInterval call is cleared
         ** and the promise is resolved. */
            this.browser.publish(msgTopic, hiddenTrackLabels, visibleDxLabels);
            clearInterval(resolveAttemptInterval);
            hidePromise.resolve(visibleDxLabels);
        }
    },

    /* When the controller (i.e. dynamix core code) hides tracks, it is because the tracks are
    ** not relevant anymore. Each dynamix group will update it's attribute list. For example:
    ** tracks that have just been hidden will be removed from the group list of tracks to show. */
    controllerHideTracks: function(trackToHideLabels){
        return this.hideTracks(trackToHideLabels, "/dynamix/tracks/dx/core/hidden");
    },

    /* When the view (i.e. dynamix widgets) hides tracks, it is because this has been requested
    ** by the user, though the UI (click on hide/refresh buttons). However in that case it does not
    ** mean that the tracks are not relevant anymore, just that the user does not want to see them
    ** first and foremost. So in that case, each dynamix group does not remove them from the list
    ** of tracks to to show but put them back at the end of this list, so they're shown last.. */
    viewHideTracks: function(trackToHideLabels){
        return this.hideTracks(trackToHideLabels, "/dynamix/tracks/dx/widget/hidden");
    },

    insertTracks: function(groupId, trackLabelsToAdd, anchorTrackLabel, beforeBool){
        var deferredInsertTracks = new Deferred(lang.hitch(this, function(){
            return trackLabelsToAdd;
        }));
        /* Insert tracks at a specific position (anchor) in the track container. Either before or after*/
        if (typeof trackLabelsToAdd == 'string') trackLabelsToAdd = trackLabelsToAdd.split(',');
        /* / ! \ The track object is the DIV !! Not the object returned by JBrowse... */
        var anchorTrackObject = this.browser.view._getTracks([anchorTrackLabel])[0].div;
        if (typeof anchorTrackObject === 'undefined') deferredInsertTracks.reject(anchorTrackLabel);
        /* Get the track_to_add configs from labels */
        var trackConfigs = [];
        var trackLabels = [];
        var alreadyVisible = this.browser.view.visibleTrackNames();
        array.forEach(trackLabelsToAdd, lang.hitch(this, function(tlabel){
            var tconfig = this.browser.trackConfigsByName[tlabel] || undefined;
            if (tconfig && (array.indexOf(alreadyVisible, tconfig.label) === -1)){
                trackConfigs.push(tconfig);
                trackLabels.push(tlabel);
            }
        }));
        if (trackConfigs){ // this partly comes from JBrowse src code, but we need to insert a node where we want (not only after!)
            /* Add to the trackDndWidget (track container) */
            this.browser.view.trackDndWidget.insertNodes(false, trackConfigs, beforeBool, anchorTrackObject);
            this.browser.trackListView.setTracksActive(trackConfigs);
            this.browser.view.updateTrackList();
        }
        this.browser.publish('/dynamix/tracks/dx/show/'+groupId, deferredInsertTracks, trackLabels);
        return deferredInsertTracks.promise;
    },

    showTracks: function(groupId, trackToShow /* {Array|String} of tracks with available signal */){
        /* Just add track at the last position in the container */
        var deferredShowTracks = new Deferred(function(reason){
        });
        if (typeof trackToShow == 'string') trackToShow = trackToShow.split(',');
        /* Get the configurations of those tracks */
        var toShow_confs = [];
        var toShow_labels = [];
        var alreadyVisible = this.browser.view.visibleTrackNames();
        array.forEach(trackToShow, lang.hitch (this, function(track){
            if (array.indexOf(alreadyVisible, track) === -1){
                toShow_confs.push(this.browser.trackConfigsByName[track]);
                toShow_labels.push(track);
            }
        }));
        this.browser.publish('/jbrowse/v1/c/tracks/show', toShow_confs);
        // The promise is sent for resolution
        this.browser.publish('/dynamix/tracks/dx/show/'+groupId, deferredShowTracks, toShow_labels);
        return deferredShowTracks.promise;
    },


    /***************
    ** OTHER **
    ***************/

    /* This is used often when a function has to return a promise
    ** but a speciifc condition was not met.
    **
    ** Its is also used when a special resolveValue needs to be taken further in
    ** a promise chain
    */
    newResolvedDummyPromise: function(resolveValue){
        var newResolvedDummyDeferred = new Deferred();
        newResolvedDummyDeferred.resolve(resolveValue);
        return newResolvedDummyDeferred.promise;
    },

    /**/
    dxCookie: function(dxKey, value) {
        /* Set a dxCookie if value, return the cookie value in any case
        ** The cookie is in fact a variable in the localStorage (not exactly a cookie...) */
        dxKey = "dynamix" + '-' + dxKey;
        if(typeof value == 'object') value = dojo.toJson(value);

        if(value){
            try {
                return localStorage.setItem(dxKey, value);
            }
            catch(e){
                console.error(e);
            }
        }
        return (localStorage.getItem( dxKey ));
    },

    /**/
    removeDxCookie: function(dxKey){
        dxKey = "dynamix" + '-' + dxKey;
        localStorage.removeItem(dxKey);
    }
});
});





