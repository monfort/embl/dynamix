import os
import re
from dx_other_utils import humansize
from dx_other_utils import sizesuffixes as sizesuffixes_table
import dx_other_utils

#regex for detecting a track line
re_trackline = re.compile("^(track\s.*name=)(?P<label>.*?)(\s.*)", re.IGNORECASE)


def yield_bed_file_lines(bed_file_path):
    try:
        with open(bed_file_path, 'r') as bed_file:
            for bed_file_line in bed_file:
                bed_line = bed_file_line.strip().split()
                yield bed_line
    except IOError as ioerr_exc:
        raise ioerr_exc


def read_bed_file_to_dictionary(bed_file_path):
    bed_file_name = os.path.basename(bed_file_path)
    bed_intervals = {}
    print "### Reading {0} [...]".format(bed_file_name)
    try:
        file_size = dx_other_utils.get_file_size(bed_file_path)
        if file_size.upper().endswith(tuple(sizesuffixes_table[2:])):
            print "### [WARN] {0} is {1}.".format(bed_file_name, file_size)
        for splitted_bed_file_line in yield_bed_file_lines(bed_file_path):
            c_bed_file_line = []
            if not re_trackline.match("\t".join(splitted_bed_file_line)):
                try:
                    c_bed_file_line = splitted_bed_file_line[0:4]
                    c_bed_file_line_names = c_bed_file_line[3].split(",")
                except IndexError as colt3_bed:
                    c_bed_file_line_names = ["nonamefound"]
                for line_name in c_bed_file_line_names:
                    # Create a default entry for the chr and for the interval
                    # Associate all known names to this interval (in a set of unique names)
                    bed_intervals.setdefault(splitted_bed_file_line[0], {}).setdefault(splitted_bed_file_line[1]+'-'+splitted_bed_file_line[2], {}).setdefault('col_name', set()).add(line_name)
            else:
                print "### Ignoring trackline"
                continue
        print "### Reading {0} [Done]".format(bed_file_name)
    except (OSError, IOError) as read_bed_oserr:
        raise read_bed_oserr
    return bed_intervals


def overwrite_bed_file_with_dic(bed_file, bed_dic):
    bed_file_name = os.path.basename(bed_file)
    print "### Writing {0} [...]".format(bed_file_name)
    try:
        with open(bed_file, 'w') as ovrwrt_bed_file:
            sorted_chrids = sorted(bed_dic.keys())
            print "### For",
            if not sorted_chrids: print "(No locations)",
            for chrid in sorted_chrids:
                print chrid,
                sorted_intervals = sorted(bed_dic[chrid].keys())
                for genomic_interval in sorted_intervals:
                    col_feature = set(bed_dic[chrid][genomic_interval].get("col_name", []))
                    if col_feature:
                        ovrwrt_bed_file.write("\t".join([chrid] + genomic_interval.split('-') + [",".join(col_feature)]) + "\n")
    except IOError as write_bed_ioerr:
        print "### [ERROR] Writing {0}.".format(bed_file_name)
        raise write_bed_ioerr
    print "\n### Writing {0} [Done]".format(bed_file_name)
    return


# def filter_bed_lines(dx_locations_file_lines, loc_trackname, tracklist_tracklabels):
#     ''' Filter lines out if the 4th column is not associated to a known track label in tracklist.json '''
#     filtered_dx_locations_lines = []
#     tracklabels = set()
#     for location_line in dx_locations_file_lines:
#         sp_loc_line = re.split('\s+', location_line.strip())
#         loc_name = ""
#         if len(sp_loc_line) >= 4:
#             if sp_loc_line[3] in tracklist_tracklabels:
#                 loc_name = sp_loc_line[3]
#             else:
#                 loc_name = loc_trackname
#         elif len(sp_loc_line) >= 3:
#             loc_name = loc_trackname
#         else:
#             continue
#         locations_line = '\t'.join(sp_loc_line[0:3] + [loc_name])
#         tracklabels.add(loc_name)
#         filtered_dx_locations_lines.append(locations_line)
#         #
#     return filtered_dx_locations_lines, tracklabels
