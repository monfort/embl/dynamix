import sys
import os
import re
import time
import dx_utils.jbrowse_conf_parser as jbrowse_conf_parser
import dx_utils.jbrowse_json_handler as jbrowse_json_handler
import dx_utils.jbrowse_bed_parser as jbrowse_bed_parser
import dx_utils.jbrowse_index_utils as jbrowse_index_utils
import dx_utils.jbrowse_raw_utils as jbrowse_raw_utils
import dx_utils.dx_other_utils as dxu
try:
    import argparse
except ImportError as arg_imp_err:
    sys.exit("### [Import Error] {0}.The 'argparse' library needs to be installed.".format(arg_imp_err))

__PLUGIN_NAME = "dynamix"

# Default JBROWSE and DYNAMIX conf path, relative to JBrowse instance root
__CURRENT_DIR = os.environ['PWD']
__DEFAULT_JBROWSE_DIR = "../../../"
__DATA_ROOT_KEYWORD = "data"  # default here, changed later on

__ALLOWED_SIGNAL_FILE_EXT = ('.bigwig', '.bw') # do not capitalise
__ALLOWED_LOC_FILE_EXT = ('.bed', '.broadpeak', '.narrowpeak')  # do not capitalise
__FEATURES_PRIORITY_TAGS = range(2, 100)


def is_valid_signal_file(signal_file_name_ext):
    signal_file_name, signal_file_ext = os.path.splitext(signal_file_name_ext)
    if signal_file_ext.lower() not in __ALLOWED_SIGNAL_FILE_EXT:
        print "Ignoring file {0} because {1} is not supported format for signal.".format(signal_file_name_ext, signal_file_ext[1:])
        return False
    return True


def is_valid_loc_file(loc_file_name_ext):
    loc_file_name, loc_file_ext = os.path.splitext(loc_file_name_ext)
    if loc_file_ext.lower() not in __ALLOWED_LOC_FILE_EXT:
        print "Ignoring file {0} because {1} is not supported format for features.".format(loc_file_name_ext, loc_file_ext[1:])
        return False
    return True


def get_fname_noext(file_name_ext):
    file_name, file_ext = os.path.splitext(file_name_ext)
    return file_name

def generate_jbrowse_track_label(file_name_ext, suffix=""):
    file_name = get_fname_noext(file_name_ext)
    jbrowse_track_label = file_name.replace(" ", "_")
    jbrowse_track_label += suffix
    return jbrowse_track_label


def normalise_path(path):
    if not os.path.isabs(path):
        path = os.path.join(__CURRENT_DIR, path)
    return os.path.normpath(path)


def safe_dataroot_path(path, data_root_keyword):
    #replace KW with configuration values
    return path.replace('{dataRoot}', data_root_keyword)


def is_valid_jbrowse_directory(jbrowse_directory):
    ''' Check JBrowse instance root directory
    Return the full path or raise exception'''
    is_valid = False
    # Declare two sets of string mimicking the content of two JBrowse key folders
    jbrowse_bin_dir_safeset = set(["add-bam-track.pl", "add-bw-track.pl", "add-track-json.pl",
                                   "flatfile-to-json.pl", "prepare-refseqs.pl", "remove-track.pl"])
    jbrowse_src_dir_safeset = set(["dojo", "JBrowse"])
    # jbrowse_dir_safeset = set(["jbrowse.conf"])
    try:
            jbrowse_src_dir = set(os.listdir(os.path.join(jbrowse_directory, 'src')))
            jbrowse_bin_dir = set(os.listdir(os.path.join(jbrowse_directory, 'bin')))
            # jbrowse_dir = set(os.listdir(jbrowse_directory))
    except Exception as listdir_exc:
        print "Encountered exception : [", type(listdir_exc), "]", listdir_exc
    if jbrowse_src_dir.intersection(jbrowse_src_dir_safeset) and \
            jbrowse_bin_dir.intersection(jbrowse_bin_dir_safeset):
            # and jbrowse_dir.intersection(jbrowse_dir_safeset):
        is_valid = True
    if not is_valid:
        print "### [WARN] The directory {0} does not seem to be a valid JBrowse install".format(jbrowse_directory)
    return is_valid


def read_jbrowse_configuration_file(jbrowse_config_path):
    raw_configuration_content = []
    config_bname = os.path.basename(jbrowse_config_path)
    try:
        with open(jbrowse_config_path, 'r') as jbrowse_config:
            raw_configuration_content = jbrowse_config.readlines()
    except IOError as read_ioerr:
        raise read_ioerr
    return raw_configuration_content



def parse_config_file(jbrowse_conf_file_path):
    '''
    Parse configuration files.
    Try reading files first with a json parser, then with our own conf parser.
    Exception occurs when parsing fails (e.g. json parser trying to parse a different format),
    but it is expected, then handled, to fail on at least one of the two.
    Both parsers return a dictionary containing the configuration (or empty)
    NB: not sure about what json parser returns but works like a dict.
    Return a list of 2 elements tuples: first element is always the configuration file path
    and the second element is the parsed configuration content .
    '''
    jbrowse_config = {}
    jbrowse_raw_config = {}
    try:
        jbrowse_raw_config = read_jbrowse_configuration_file(jbrowse_conf_file_path)
    except IOError as jbrowse_raw_ioerr:
        print "### [DEBUG] IOError : ", jbrowse_raw_ioerr
        raise jbrowse_raw_ioerr
    else:
        try:
            jbrowse_config = jbrowse_json_handler.parse_json_format(jbrowse_raw_config)
        except ValueError as raw_config_val_err:
            print "### [DEBUG] ValueError has been raised when parsing {0} : {1}".format(os.path.basename(jbrowse_conf_file_path),
                                                                                                                                         raw_config_val_err)
            print "### [DEBUG] Try parsing JBrowse Conf format instead ..."
            try:
                jbrowse_config = jbrowse_conf_parser.get_jbrowse_conf_dictionary(jbrowse_raw_config)
            except SyntaxError as json_parse_syntaxerr:
                print "### [DEBUG] SyntaxError has been raised when parsing {0} : {1}".format(os.path.basename(jbrowse_conf_file_path),
                                                                                                                                                json_parse_syntaxerr)
                print "### [WARN] {0} has an unknown format.".format(os.path.basename(jbrowse_conf_file_path))
            except KeyError as json_parse_kerr:
                print "### [DEBUG] KeyError has been raised when parsing  {0} : {1}".format(os.path.basename(jbrowse_conf_file_path),
                                                                                                                                           json_parse_kerr)
    return jbrowse_config



def get_config_files_content(jbrowse_conf_include_files):
    '''
    Parse configuration files.
    '''
    # get_jbrowse_tracklist_content
    jbrowse_conf_include_files_content = []
    for jbrowse_conf_file_path in jbrowse_conf_include_files:
        try:
            jbrowse_config = parse_config_file(jbrowse_conf_file_path)
        except IOError as jbrowse_raw_ioerr:
            print "### [DEBUG] IOError : ", jbrowse_raw_ioerr
            raise jbrowse_raw_ioerr
        else:
            jbrowse_conf_include_files_content.append((jbrowse_conf_file_path, jbrowse_config))
    return jbrowse_conf_include_files_content



def read_json_configuration(json_file_path):
    jbrowse_config = {}
    jbrowse_raw_config = {}
    try:
        jbrowse_raw_config = read_jbrowse_configuration_file(json_file_path)
    except IOError as jbrowse_raw_ioerr:
        raise jbrowse_raw_ioerr
    else:
        try:
            jbrowse_config = jbrowse_json_handler.parse_json_format(jbrowse_raw_config)
        except ValueError as raw_config_val_err:
            raise raw_config_val_err
    return jbrowse_config



def get_tracks_configurations(config_files_path_content_tuples):
    ''' Iterates on configuration content's tuples to exctract the lists of tracks registered in
    jbrowse configuration with the 'tracks' kw. This list contains all the tracks configurations
    as anonymous dictionaries e.g. like in trackList.json.
    Return all the found track config lists concatenated and their path (as a list of path)
    '''
    tracks_configurations_paths = set()
    all_tracks_configurations = []
    for config_path, config_content in config_files_path_content_tuples:
        config_tracks = config_content.get('tracks', [])
        if config_tracks:
            all_tracks_configurations += config_tracks
            tracks_configurations_paths.add(config_path)
    return all_tracks_configurations, tracks_configurations_paths


# def reset_dynamix_tracklist_priority_tags(tracklist_path):
#     print "### Reseting dynamix tracks priority..."
#     try:
#         tracklist_content = read_json_configuration(tracklist_path)
#     except (OSError, IOError, ValueError) as read_tracklist_exc:
#         raise read_tracklist_exc
#     else:
#         config_tracks = tracklist_content.get('tracks', [])
#         if config_tracks:
#             new_config_tracks = recompute_config_priority_tags(config_tracks)
#             tracklist_content['tracks'] = new_config_tracks
#             jbrowse_json_handler.write_json_file(tracklist_path, tracklist_content)


# def recompute_config_priority_tags(config_tracks):
#     config_known_dxids_map = {}
#     for track_conf in config_tracks:
#         if 'dynamix_id' in track_conf:
#             dynamix_id = track_conf['dynamix_id']
#             try:
#                 track_label = track_conf['label']
#                 track_ptag = track_conf['dynamix_priority']
#             except KeyError as keyerr:
#                 raise keyerr
#             else:
#                 config_known_dxids_map.setdefault(dynamix_id, {})[track_label] = track_ptag
#     # iterate once, add a sorted_list of priority digits
#     for dynamix_id, dynamix_info in config_known_dxids_map.items():
#         sorted_priority_digits = sorted(list(set(dynamix_info.values() - set(1, 2))))
#         config_known_dxids_map[dynamix_id]['sorted_priority_digits'] = sorted_priority_digits
#     for track_conf in config_tracks:
#         # print track_conf
#         if 'dynamix_id' in track_conf:
#             dynamix_id = track_conf['dynamix_id']
#             dynamix_label = track_conf['label']
#             dynamix_ptag = track_conf['dynamix_priority']
#             dynamix_type = track_conf['type']
#             if dynamix_type == 'JBrowse/View/Track/Wiggle/XYPlot': track_conf['dynamix_priority'] = 1
#             elif dynamix_type == 'JBrowse/View/Track/Wiggle/Density': track_conf['dynamix_priority'] = 2
#             else:
#                 sorted_priority_digit = config_known_dxids_map[dynamix_id]['sorted_priority_digits']
#                 real_priority = sorted_priority_digit.index(dynamix_ptag) # index in the sorted list
#                 real_ptag = __FEATURES_PRIORITY_TAGS[real_priority]
#                 track_conf['dynamix_priority'] = real_ptag
#     return config_tracks

# def set_default_dynamix_configuration(dynamix_configuration):
#     ''' Similar to get_tracks_configurations() but for Dynamix configuration registered in with the 'dynamix' keyword
#     Exctract the value associated to 'dynamix' but associate it again with the same key. Just to be sure it is a dictionary
#     with only one key that is the 'dynamix' key.
#     If we don't find the Dynamix configuration iterating on included configs, we get a default one (for->else<-)
#     Return the Dynamix configuration and its path.
#     '''
#     dx_configuration = dynamix_configuration.get('dynamix', {})
#     # In case user screwed configuration, set some default values
#     dx_configuration.setdefault("default_state", "active")
#     dx_configuration.setdefault("group_configurations", {})
#     return { 'dynamix' : dx_configuration }


def set_default_empty_main_dynamix_configuration(dynamix_configuration):
    '''
    '''
    dynamix_configuration.setdefault('include', []) # to include additionnal tracks definitions
    dynamix_configuration.setdefault('dynamix', {}).setdefault("default_state", "active")
    dynamix_configuration.setdefault('dynamix', {}).setdefault("global_max_tracks", "100")
    dynamix_configuration.setdefault('dynamix', {}).setdefault("group_configurations", {})
    return dynamix_configuration


def set_default_dynamix_group_configuration(dynamix_configuration, group_id, included_confs, out_tracklist, dynamix_dir):
    ''' '''
    ## Include the output tracklist, if not already known
    dynamix_configuration.setdefault('include', [])
    if out_tracklist not in included_confs:
        dx_include_set = set(dynamix_configuration.get('include', []))
        dx_include_set.add(os.path.relpath(out_tracklist, dynamix_dir))
        dynamix_configuration['include'] = list(dx_include_set)
    ## define a group config
    dynamix_configuration.setdefault('dynamix', {}).setdefault('group_configurations', {}).setdefault(group_id, {})
    dynamix_group_dir = os.path.dirname(out_tracklist)
    location_bed_file_name_prefix = group_id+"_locations"
    full_group_location_path = os.path.join(dynamix_group_dir, location_bed_file_name_prefix+'.bed')
    group_location_path = os.path.relpath(full_group_location_path, dynamix_dir)
    dynamix_configuration['dynamix']['group_configurations'][group_id].setdefault("view_anchor", ["after", "trackLabel"])
    dynamix_configuration['dynamix']['group_configurations'][group_id].setdefault("show_features", "true")
    dynamix_configuration['dynamix']['group_configurations'][group_id].setdefault("show_signal", "true")
    dynamix_configuration['dynamix']['group_configurations'][group_id].setdefault("overlap_range", "5")
    dynamix_configuration['dynamix']['group_configurations'][group_id].setdefault("order", "alpha")
    dynamix_configuration['dynamix']['group_configurations'][group_id].setdefault("max_tracks", "30")
    dynamix_configuration['dynamix']['group_configurations'][group_id].setdefault("mode", "manual")
    dynamix_configuration['dynamix']['group_configurations'][group_id].setdefault("group_track_ids", "")
    dynamix_configuration['dynamix']['group_configurations'][group_id].setdefault("group_track_name", group_id)
    dynamix_configuration['dynamix']['group_configurations'][group_id].setdefault("dynamix_data", group_location_path)
    dynamix_group_data_bed_file = dynamix_configuration['dynamix']['group_configurations'][group_id]["dynamix_data"]
    dynamix_group_data_bed_trackname = dynamix_configuration['dynamix']['group_configurations'][group_id]["group_track_name"]
    if not os.path.isabs(dynamix_group_data_bed_file):
        dynamix_group_data_bed_file = os.path.join(dynamix_dir, dynamix_group_data_bed_file)
    return dynamix_configuration, os.path.normpath(dynamix_group_data_bed_file), dynamix_group_data_bed_trackname


# def set_default_dynamix_group_configuration(dx_configuration, dx_tracklist_path, jbrowse_dir, group_id):
#     config = dx_configuration['dynamix']
#     config['group_configurations'].setdefault(group_id, {})
#     dx_group_bed_track_name = "dynamix_locations_"+group_id
#     dynamix_tracklsit_dir = os.path.dirname(dx_tracklist_path)
#     full_group_location_path = os.path.join(dynamix_tracklsit_dir, dx_group_bed_track_name+'bed')
#     dx_group_loc_file = os.path.relpath(jbrowse_dir, full_group_location_path)
#     config['group_configurations'][group_id].setdefault("overlap_range", "0")
#     config['group_configurations'][group_id].setdefault("order", "list")
#     config['group_configurations'][group_id].setdefault("max_tracks", "25")
#     config['group_configurations'][group_id].setdefault("mode", "enabled")
#     config['group_configurations'][group_id].setdefault("group_track_ids", "")
#     config['group_configurations'][group_id].setdefault("group_track_name", dx_group_bed_track_name)
#     config['group_configurations'][group_id].setdefault("dynamix_data", dx_group_loc_file)
#     return { 'dynamix' : config }


def get_a_specific_track_configurations(tracklabel, tracklist_object):
    if 'tracks' in tracklist_object:
        rev_tracklist = reversed(tracklist_object['tracks'])
        for track_conf in rev_tracklist:
            if not 'label' in track_conf:
                # print "No label found in the track conf"
                continue
            elif tracklabel == track_conf['label']:
                return track_conf
    else:
        print "# Not a valid JBrowse tracklist format"


def update_group_track_ids(dynamix_configuration, dx_group_id, added_ids):
    before_gids = set(dynamix_configuration.get('dynamix', {}).get("group_configurations", {}).get(dx_group_id, {}).get("group_track_ids", "").split(","))
    after_gids = before_gids | set(added_ids)
    dynamix_configuration['dynamix']['group_configurations'][dx_group_id]['group_track_ids'] = ",".join([gid for gid in after_gids if gid])
    return dynamix_configuration


def add_dynamix_dataset(jbrowse_directory, dynamix_group_dir, tracklist, dx_dataset_path, display_name, dynamix_id, dx_xyplot,  xyplot_height, xyplot_poscolor, xyplot_negcolor, dx_heatmap, heatmap_height, heatmap_poscolor, heatmap_negcolor, smin, smax, autoscaling):
    ''' Add new datasets to JBrowse tracklist. Gets the dataset paths indicated in cmd line by user
    and create the associated configuration using JBrowse perl scripts.
    First get a list of file path from what has been idicated by user (a single file path or a directory)
    !! IMPROVE ME !! Handle both XYplot and heatmaps for bigwig datasets -- ask user to flag in cmd which type he wants
    !! IMPROVE ME !! set a dynamix_priority_tag parameter in track config a,b,c,d,e ...
                                signal file XYPLOT == a
                                signal file HitMap == b
    '''
    # print jbrowse_directory, dx_dataset_path, dynamix_id, dx_xyplot, dx_heatmap
    dx_dataset_path = normalise_path(dx_dataset_path)
    # For now ignoring all that is not a bigwig (.bigwig or .bw).
    # Need to consider to work with BAM files, but this would slow down the whole process.
    dx_dataset_path_file_name_ext = os.path.basename(dx_dataset_path)
    print "### Given data file [-s/--dx-data-file]: {0}".format(dx_dataset_path)
    if os.path.isfile(dx_dataset_path) and is_valid_signal_file(dx_dataset_path_file_name_ext):
        if dx_xyplot or dx_heatmap:
            try:
                if dx_xyplot:
                    add_dynamix_signal_track_configuration(jbrowse_directory, dynamix_group_dir, tracklist,
                                                                                        dx_dataset_path, "XYplot",  ['--plot'], display_name, dynamix_id, 1,
                                                                                        xyplot_height, xyplot_poscolor, xyplot_negcolor, smin, smax, autoscaling)
                if dx_heatmap:
                    add_dynamix_signal_track_configuration(jbrowse_directory, dynamix_group_dir, tracklist,
                                                                                        dx_dataset_path, "Heatmap", [], display_name, dynamix_id, 2,
                                                                                        heatmap_height, heatmap_poscolor, heatmap_negcolor, smin, smax, autoscaling)
            except (OSError, RuntimeError) as add_dataset_err:
                raise add_dataset_err
            else:
                return dynamix_id
        else:
            print "### Nothing to do for {0}".format(dx_dataset_path)
            print "### Missing -m (heatmap) or -x (xy plot) flag in command line.".format(dx_dataset_path_file_name_ext)
    else:
        print "# Error: Indicated -s/--dx-data-file {0} is not a valid file".format(dx_dataset_path)



def add_dynamix_signal_track_configuration(jbrowse_directory, dynamix_group_dir, tracklist,
                                           dataset_path, track_type_str, track_type_extra_arg, display_name, dynamix_id, track_priority,
                                           track_height, track_poscolor, track_negcolor, smin, smax, autoscaling):
                    # add_dynamix_signal_track_configuration(track_configurations, jbrowse_directory, tracklist,
                    #                                                                     dx_dataset_path, "XYplot",  ['--plot'], display_name, dynamix_id, 'a',
                    #                                                                     xyplot_height, xyplot_poscolor, xyplot_negcolor, smin, smax, autoscaling)

    global __REPLACE
    replace = __REPLACE
    dataset_name = os.path.basename(dataset_path)
    track_type_suffix = "_"+track_type_str
    # get tracklabels once instead of each time we're adding a signal file
    print "### [...] Configuring {0} track".format(track_type_str)
    signal_tracklabel = generate_jbrowse_track_label(dataset_name, track_type_suffix)
    if not display_name: display_name = signal_tracklabel.replace("_", " ")
    else: display_name = generate_jbrowse_track_label(display_name, track_type_suffix).replace("_", " ")
    existing_tracklabels = get_tracklist_tracklabels(tracklist)
    track_exists = True if signal_tracklabel in existing_tracklabels else False
    if track_exists:
        # If track is known, ask user if he wants to replace the configuration
        print "### [WARN] The track with label \"{0}\" is already known. ".format(signal_tracklabel)
        print "### [WARN] Overwriting existing track configuration ?",
        if not replace: replace = dxu.validate_usr_yes_answer()
        else: print replace
    if track_exists and replace:
        # remove-track.pl --trackLabel MyTrackLabel
        if replace == 'all':
            __REPLACE = True
        ### :: FIX ::
        ### Removing the config snippet from the config should not be necessary
        ### since the config should be replaced since it has the same label.
        ### However there seems to be a bug (The script adds an empty entry in the tracklist)
        ### that does not occur when the config snippet has previously been removed
        ### NB: I could not reproduce the bug consistently
        remove_tr_path = os.path.join(jbrowse_directory, 'bin', 'remove-track.pl')
        dxu.call_jbrowse_script(remove_tr_path, ['--trackLabel', signal_tracklabel, '--dir', os.path.dirname(tracklist), '-q'])
        ## END FIX
    if (track_exists and replace) or not track_exists:
        # Create the new configuration only if the track does not exist or a replacement is asked by user
        try:
            dynamix_id = add_signal_file(jbrowse_directory, dynamix_group_dir, tracklist, dataset_path, dynamix_id, signal_tracklabel, display_name, track_priority, track_type_extra_arg, track_height, track_poscolor, track_negcolor, smin, smax, autoscaling)
        except (OSError, RuntimeError) as add_sign_exc:
            print "### [ERROR] Failed to configure a {0} track.".format(track_type_str)
            raise add_sign_exc
        else:
            print "### [Done] Configuring {0} track".format(track_type_str)
    return dynamix_id


def add_signal_file(jbrowse_directory, dynamix_group_dir, tracklist, dx_dataset, dynamix_id, signal_tracklabel, display_name, dynamix_priority, extra_args, track_height, track_poscolor, track_negcolor, smin, smax, autoscaling):
    # dynamix_id = add_signal_file(jbrowse_directory, tracklist, dataset_path, dynamix_id, signal_tracklabel, display_name, track_priority, track_type_extra_arg, track_height, track_poscolor, track_negcolor, smin, smax, autoscaling)
    ''' 2 steps, create a tmp tracklist '''
    timestr = time.strftime("%Y%m%d-%H%M%S")
    tm_tmp_filename = timestr+signal_tracklabel[:20]
    add_bw_track_script_path = os.path.join(jbrowse_directory, "bin", "add-bw-track.pl")
    add_track_conf_script_path = os.path.join(jbrowse_directory, "bin", "add-track-json.pl")
    # First add the bigwig configuration to the existing (input) trackList (in_tracklist).
    # Output the result in a temporary tracklist (/tmp/tmp_trackList.json) because it does
    # not contain the full configuration of a dynamix track
    try:
        jbrowse_raw_utils.call_jbrowse_bigwig_script(add_bw_track_script_path,
                                                                                  os.path.relpath(dx_dataset, os.path.dirname(tracklist)),
                                                                                  signal_tracklabel,
                                                                                  tracklist,
                                                                                  '/tmp/tmp_trackList.json',
                                                                                  extra_args)
    except (OSError, RuntimeError) as call_create_conf_oserr:
        raise call_create_conf_oserr
    else:
        # The intermediate configuration was succesfully created
        # NB: Hopefully will be able to get rid of this piece of code when bigwig perl script will have a --config param
        # Read tmp config
        updated_tracklist_content = read_jbrowse_configuration_file('/tmp/tmp_trackList.json')
        updated_tracklist = jbrowse_json_handler.parse_json_format(updated_tracklist_content)
        # print "dumped...updated_tracklist\n", jbrowse_json_handler.dump_pretty_json(updated_tracklist)
        # Get the newly created track configuration in the JSON object (parsed from tmp tracklist)
        signal_file_conf = get_a_specific_track_configurations(signal_tracklabel, updated_tracklist)
        # Manually adding pieces of config to track configuration
        signal_file_conf['key'] = display_name
        signal_file_conf['urlTemplate'] = os.path.relpath(dx_dataset, os.path.dirname(tracklist))
        signal_file_conf['data_file'] = os.path.relpath(dx_dataset, os.path.dirname(tracklist))
        print "SIGNAL:: data_file_string :: ", signal_file_conf['data_file']
        signal_file_conf['dynamix_id'] = dynamix_id
        signal_file_conf['dynamix_priority'] = dynamix_priority
        signal_file_conf['category'] = 'Signal'
        if autoscaling is not None: signal_file_conf['autoscale'] = str(autoscaling)
        if smin is not None: signal_file_conf['min_score'] = smin
        if smax is not None: signal_file_conf['max_score'] = smax
        signal_file_conf.setdefault("style", {}).update({"height": track_height})
        signal_file_conf.setdefault("style", {}).update({"pos_color": track_poscolor})
        signal_file_conf.setdefault("style", {}).update({"neg_color": track_negcolor})
        # Write out track configuration in a specific tmp file, for easily adding it with "add-track-json.pl"
        jbrowse_json_handler.write_json_file('/tmp/'+tm_tmp_filename, signal_file_conf)
        # Call "add-track-json.pl" to finally add the config to the real output tracklist
        try:
            stdout = dxu.call_jbrowse_script(add_track_conf_script_path, ['/tmp/'+tm_tmp_filename, tracklist])
        except (OSError, RuntimeError) as call_add_conf_oserr:
            print "###", repr(call_add_conf_oserr), call_add_conf_oserr
            print "### /tmp/{0} will be kept. It can be added manually using JBrowse {1}".format(tm_tmp_filename,
                                                                                                                                                 os.path.basename(add_bw_track_script_path))
            raise call_add_conf_oserr
        else:
            # print "### [Done] Wrote {0} dynamix track configuration to trackList.".format(signal_tracklabel)
            # print "### Removing tmp trackList /tmp/tmp_trackList.json"
            os.remove('/tmp/'+tm_tmp_filename)
            os.remove("/tmp/tmp_trackList.json")



def update_dynamix_locations_dic(dynamix_locations_dic, added_dxfeatures_file_dic, dx_signal_id, dx_feature_id):
    ''' Create dynamix locations lines from dx features files indicated in cmd line. It associates the locations present in each line of the
    file (col0, col1, col2) to the dynamix ID of the feature track - and of the signal track if present. It ignores the rest of the columns in
    the file. New lines are added to the existing lines of the group dyanmix_locations file.
    Returns a dictionary following the dynamix_locations dic format i.e. { "chr" : { "coordA-coordB" : [ ID1, ID2 ] } }
    '''
    valid_lines_ids = [ id for id in [dx_signal_id, dx_feature_id] if id]
    chrids = sorted(added_dxfeatures_file_dic.keys())
    if chrids:
        print "### Updating Dynamix locations for",
        for chrid in chrids:
            dx_genomic_intervals = added_dxfeatures_file_dic[chrid].keys()
            if not dx_genomic_intervals: continue
            print chrid,
            for genomic_interval in dx_genomic_intervals:
                for vid in valid_lines_ids:
                    dynamix_locations_dic.setdefault(chrid, {}).setdefault(genomic_interval, {}).setdefault("col_name", set()).add(vid)
        print "[Done]"
    return dynamix_locations_dic


def add_dynamix_locations(dynamix_locations_dic, dx_feature_file, dx_signal_id, dx_feature_id):
    dx_features_to_add_dic = jbrowse_bed_parser.read_bed_file_to_dictionary(dx_feature_file)
    dynamix_locations_dic = update_dynamix_locations_dic(dynamix_locations_dic, dx_features_to_add_dic, dx_signal_id, dx_feature_id)
    return dynamix_locations_dic


def get_dynamix_config_info(jbrowse_data_dynamix_dir, dynamix_configuration):
    ''' Look up into  the dyanmix configuration object.
    Return the dynamix data file (data/dynamix/data/dynamix_locations.bed), the dynamix data dir (data/dyanmix/data/),
    and the dyanmix track name ("dynamix_locations").
    '''
    dynamix_data_file_path = os.path.join(jbrowse_data_dynamix_dir, dynamix_configuration.get('dynamix', {}).get('dynamix_data', ""))
    dynamix_data_dir, filename = os.path.split(dynamix_data_file_path)
    dynamix_trackname, ext = os.path.splitext(filename)
    return dynamix_data_file_path, dynamix_data_dir, dynamix_trackname


def remove_a_dx_track(dx_trackname, jbrowse_data_dir, jbrowse_directory, dx_group_configuration, dynamix_bed_content_lines):
    ''' Rmove a dynamix track from the dynamix configuration AND the associated lines from the dynamix locations file.
    Then this track won't be handled dynamically anymore.
    '''
    dxu.call_jbrowse_script("remove-track.pl", ['--trackLabel', dx_trackname, '--dir', jbrowse_data_dir], jbrowse_directory)
    # remove from group trakcklist
    group_tracklist = dx_group_configuration.get('group_track_ids', "").split(",")
    try:
        group_tracklist.remove(dx_trackname)
    except ValueError as verr:
        print "{0} has not been found in tracklist. Err: {1}".format(dx_trackname, verr)
    else:
        print "{0} has been removed from tracklist".format(dx_trackname)
        dx_group_configuration['group_track_ids'] = ",".join(group_tracklist)
        re_trackname = re.compile("(.*"+dx_trackname+")$")
        # print re_trackname.pattern
        dynamix_bed_content_lines = [bed_l for bed_l in dynamix_bed_content_lines if not re_trackname.match(bed_l)]
    return dx_group_configuration, dynamix_bed_content_lines


# def base_group_init(dynamix_configuration_path, dynamix_data_file_path):
#     if not os.path.isdir(os.path.dirname(dynamix_data_file_path)):  #data/dynamix
#         os.makedirs(os.path.dirname(dynamix_data_file_path))
#     if not os.path.isfile(dynamix_data_file_path):
#         jbrowse_json_handler.write_json_file(dynamix_configuration_path)
#     if not os.path.isfile(dynamix_data_file_path):
#         open(dynamix_data_file_path, 'w').close()


def is_features_file_in_tracklist(features_track_label_prefix, tracklist_tracklabels):
    #prefix has _f in, so it is dediacted to features files, no signal files
    re_trcklabel_prefix = re.compile('('+features_track_label_prefix+'$)')
    known_label = False
    for label in tracklist_tracklabels:
        match_known_name = re_trcklabel_prefix.match(label)
        if match_known_name:
            known_label = match_known_name.group(0)
    return known_label


def generate_unique_features_track_dynamix_id(dx_id, track_configurations):
    count = 2 # take into account a max of two signal tracks
    # Gather all the known priority tags
    for track_conf in track_configurations:
    # for known_label, known_id, known_ptag in tracklist_tracklabel_dxid_ptag_tuplist:
        dynamix_id = track_conf.get('dynamix_id', None)
        dynamix_priority = track_conf.get('dynamix_priority', None)
        if (dynamix_id and dynamix_priority):
            if str(dx_id) == str(dynamix_id): count+=1
    first_free = __FEATURES_PRIORITY_TAGS[count+1]
    print "### Setting {0} priority to {1}".format(dx_id, first_free)
    return dx_id_prefix+first_free, first_free


def compute_unique_pritority_tag(dx_id, track_configurations):
    unique_max_ptag = None
    dxid_prefix = dx_id+"_"
    known_priority_tags = [ 0 ]
    # THIS PREFIX SHOULD BE SPECIFIC TO FEATURE TRACKS
    re_dxid_prefix = re.compile('(?P<idprefix>'+dxid_prefix+')(?P<idptag>[0-9]+)')
    # Gather all the known priority tags
    for track_conf in track_configurations:
        dynamix_id = track_conf.get('dynamix_id', '')
        match_known_id = re_dxid_prefix.match(dynamix_id)
        if match_known_id:
            ptag = track_conf.get('dynamix_priority', 0)  # 3 mini...
            known_priority_tags.append(ptag)
    try:
        unique_max_ptag = __FEATURES_PRIORITY_TAGS[int(max(known_priority_tags))+1]
    except Exception as exc_tag:
        try:
            unique_max_ptag = __FEATURES_PRIORITY_TAGS[len(known_priority_tags) + 1]
        except Exception as exc_tag2:
            print "### Could not determine a unique priority tag for ", dx_id
            print "###", exc_tag, repr(exc_tag), exc_tag2, repr(exc_tag2)
    print "### Setting {0} priority to \"{1}\"".format(dx_id, unique_max_ptag)
    return dx_id, unique_max_ptag


# def generate_unique_features_track_dynamix_id(dx_id, track_configurations):
#     dx_id_prefix = dx_id+'_f'
#     re_dxid_prefix = re.compile('(?P<idprefix>'+dx_id_prefix+')(?P<idptag>[a-z])')
#     known_ids_ptags = []
#     known_ptags = set()
#     # Gather all the known priority tags
#     for track_conf in track_configurations:
#     # for known_label, known_id, known_ptag in tracklist_tracklabel_dxid_ptag_tuplist:
#         dynamix_id = track_conf.get('dynamix_id', None)
#         dynamix_priority = track_conf.get('dynamix_priority', None)
#         if (dynamix_id and dynamix_priority):
#             known_priority_tag.add()
#         match_known_id = re_dxid_prefix.match(known_id)
#         if match_known_id:
#             known_label = match_known_id.group(0)
#             known_ptags.add(match_known_id.group('idptag'))
#     first_free = sorted(list(set(__FEATURES_PRIORITY_TAGS) - known_ptags - set(['a', 'b'])))[0]
#     print "### Setting {0} priority to \"{1}\"".format(dx_id, first_free)
#     return dx_id_prefix+first_free, first_free


def add_dynamix_feature(feature_file_path, tracklist, jbrowse_directory, feature_dx_id, dynamix_group_dir):
    global __REINDEX
    replace = __REINDEX
    feature_file_path = normalise_path(feature_file_path)
    feature_file_name = os.path.basename(feature_file_path)
    if os.path.isfile(feature_file_path) and is_valid_loc_file(feature_file_path):
        print "### [Indexing] {0}.".format(os.path.basename(feature_file_path))
        #
        feature_tracklabel = generate_jbrowse_track_label(feature_file_name)
        # Re-read all track confs
        existing_tracklabels = get_tracklist_tracklabels(tracklist)
        # Check if already exists
        feature_track_exists = True if feature_tracklabel in existing_tracklabels else False
        if feature_track_exists:
            # If track is known, ask user if he wants to replace the configuration
            print "### [WARN] The feature track with label \"{0}\" is already known. ".format(feature_tracklabel)
            print "### [WARN] Overwriting existing track configuration (and index!) ?"
            print "### [WARN] /!\\ THIS WILL DELETE THE EXISTING INDEX /!\\",
            if not replace: replace = dxu.validate_usr_yes_answer()
            else: print replace
        if feature_track_exists and replace:  # remove-track.pl --trackLabel MyTrackLabel
            if replace == 'all':
                __REINDEX = True
            ### :: BUGFIX ::
            ### Removing the config snippet from the config should not be necessary
            ### since the config should be replaced since it has the same label.
            ### However there seems to be a bug (The script adds an empty entry in the tracklist)
            ### that does not occur when the config snippet has previously been removed
            ### NB: I could not reproduce the bug consistently
            remove_tr_path = os.path.join(jbrowse_directory, 'bin', 'remove-track.pl')
            feature_index_exists = True if os.path.isdir(os.path.normpath(os.path.join(dynamix_group_dir, 'tracks', feature_tracklabel))) else False
            delete_index = ['-D'] if feature_index_exists else []
            dxu.call_jbrowse_script(remove_tr_path, ['--trackLabel', feature_tracklabel, '--dir', dynamix_group_dir, '-q'] + delete_index)
        if (feature_track_exists and replace) or not feature_track_exists:
            ### Because of BUGFIX, need to re-read the new tracklist
            track_configuration = read_tracklist(tracklist)
            ### END BUGFIX
            feature_track_dx_id, feature_priority_tag = compute_unique_pritority_tag(dx_args.id, track_configuration)
            if not feature_priority_tag:
                return False
            else:
                feature_track_dx_id = feature_track_dx_id + "_" + str(feature_priority_tag)
                print "### [Indexing] {0}.".format(os.path.basename(feature_file_path))
                try:
                    index_feature_file(jbrowse_directory, dynamix_group_dir, feature_file_path, feature_tracklabel, feature_track_dx_id, feature_priority_tag, dx_args.max_height, dx_args.display_mode, dx_args.strand_arrow, dx_args.feat_color)
                except (OSError, RuntimeError) as index_oserr:
                    raise index_oserr
                else:
                    return feature_track_dx_id
    else:
        print "# Error: Indicated -f/--dx-features {0} is not a valid file.".format(os.path.basename(feature_file_path))


def index_feature_file(jbrowse_directory, jbrowse_idx_directory, feature_file_path, features_tracklabel, dynamix_id, dx_feature_ptag, dx_feature_maxheight, dx_feature_displaymode, dx_feature_strandarrow, dx_feat_color):
    jbrowse_perl_script_path = os.path.join(jbrowse_directory, 'bin', "flatfile-to-json.pl")
    data_file_string = '"data_file": "'+os.path.relpath(feature_file_path, jbrowse_idx_directory)+'"'
    print "INDEX:: data_file_string :: ", data_file_string
    # data_file_string = ''
    config_string = '{ "category" : "Features", "dynamix_id" : "'+dynamix_id+'", "dynamix_priority" : '+str(dx_feature_ptag)+', "displayMode" : "'+str(dx_feature_displaymode)+'", "maxHeight" : '+str(dx_feature_maxheight)+' , '+data_file_string+'}'
    client_config_string = '{ "strandArrow" : '+str(dx_feature_strandarrow).lower()+', "color" : "'+str(dx_feat_color)+'"}'
    extra_args = ['--key', "("+str(dx_feature_ptag - 2)+") "+features_tracklabel.replace("_", " "), '--config', config_string, '--clientConfig', client_config_string]
    try:
        returncode = jbrowse_index_utils.call_jbrowse_bed_index_script(jbrowse_perl_script_path, feature_file_path, features_tracklabel,
                                                                                                                jbrowse_idx_directory, extra_args)
    except (OSError, RuntimeError) as index_oserr:
        raise index_oserr


def make_directory(dir_path):
    if not os.path.isdir(dir_path):
        try:
            os.makedirs(dir_path)
        except OSError as make_dir_exc:
            raise make_dir_exc

def read_tracklist(tracklist_path):
    try:
        tracklist_content = read_json_configuration(tracklist_path)
        track_configurations = tracklist_content.get('tracks', [])
    except (OSError, IOError, ValueError) as read_tracklist_exc:
        print "###", repr(read_tracklist_exc), read_tracklist_exc
        if not os.path.isfile(tracklist_path): jbrowse_json_handler.write_json_file(tracklist_path, { "tracks": []} )
        track_configurations = []
    return track_configurations

def get_tracklist_tracklabels(tracklist_path):
    track_configurations = read_tracklist(tracklist_path)
    return [track_conf['label']
                for track_conf in track_configurations
                if 'label' in track_conf]



# def check_cmd_line_args(dx_args):
#     ''' Check command line argument to see if the command line is valid.
#     Most of the philosophy is present in the init_parser function but we need this function to test if:
#     - User gave dx-locations refering to a signal file/track OR set the --index-features flag.
#     '''
#     if dx_args.signal and not (dx_args.xyplot or dx_args.heatmap):
#         sys.exit("Signal indicated in command line but -p or -H are not set. Exiting.")


def init_parser():
    dx_parser = argparse.ArgumentParser(description='Administration tool for JBrowse\'s Dynamix plugin')
    # Add dx subcommand parser then add new parsers to the subparser.
    dx_subparsers = dx_parser.add_subparsers(help="script functions - subcommands", dest='command')

    ### Add data
    add_dx = dx_subparsers.add_parser('add-dx-data', help='Add Dynamix data')
    #
    add_dx.add_argument('-g', '--dx-group', required=True, action='store', type=str,
                                        default=None, metavar="DYNAMIX_GROUP", help="dynamix group to add track to.")
    #
    add_dx.add_argument('--id', required=True, action='store', type=str,
                                        default=None, metavar="DYNAMIX_ID", help="The dynamix ID for this pool of track.")
                                        #
    add_dx.add_argument('-key', '--display-name', action='store', type=str,
                                        default=None, metavar="DISPLAY_NAME", help="The track label for this pool of track.")
    #
    add_dx.add_argument('--tracklist', required=False, action="store",  type=str,
                           default=None, metavar="<tracklist>", help="Dynamix tracklist path. Default set to jbrowse_dir/data/dynamix/[group_id]_trackList.json")

    ###### Add DX datasets
    signal_file = add_dx.add_argument_group('signal_file')
    #either a file or a track defined in tracklist
    signal_type = signal_file.add_mutually_exclusive_group(required=False)
    signal_type.add_argument('-s', '--signal', action="store", default="", metavar="BIGWIG",
                                                 help='A signal bigwig file path. Add dynamic behavior to the signal file which path is DX_DATA. Create a JBrowse track configuration for the file which path is DX_DATA. This track will be dynamically shown on the genomic intervals present in the file given with the -f / --dx-features parameter.')
    signal_type.add_argument('-t', '--track', action="store", default="", metavar="TRACKLABEL",
                                                 help='A signal bigwig file path. Add dynamic behavior to the signal file which path is DX_DATA. Create a JBrowse track configuration for the file which path is DX_DATA. This track will be dynamically shown on the genomic intervals present in the file given with the -f / --dx-features parameter.')
    signal_file.add_argument('-rconfig', '--replace-config', required=False, action="store_true",
                           default=False,  help="Force the replacement of configuration in the trackList, when a track with the same label is found. Default (flag not set) to False")
    signal_file.add_argument('--min', action="store", default=None, type=int, help='XYplot track minimum value to be graphed.')
    signal_file.add_argument('--max', action="store", default=None, type=int, help='XYplot track maximum value to be graphed.')
    signal_file.add_argument('-a', '--autoscaling', action="store", default=None, type=str, help='XYplot track auto scaling. Not relevant if min_score AND max_score are set')
    signal_file.add_argument('-x', '--xyplot', action="store_true", default=False, help='Create the XY plot dynamix configuration for a signal file.')
    signal_file.add_argument('--xyplot-height', action="store", default=50, type=int, help='XYplot track height')
    signal_file.add_argument('--xyplot-poscolor', action="store", default="blue", type=str, help='XYplot positive values color')
    signal_file.add_argument('--xyplot-negcolor', action="store", default="red", type=str, help='XYplot negative values color')
    signal_file.add_argument('-m', '--heatmap', action="store_true", default=False, help='Create the heatmap dynamix configuration for a signal file')
    signal_file.add_argument('--heatmap-height', action="store", default=25, type=int, help='Heatmap track height')
    signal_file.add_argument('--heatmap-poscolor', action="store", default="red", type=str, help='Heatmap positive values color')
    signal_file.add_argument('--heatmap-negcolor', action="store", default="blue", type=str, help='Heatmap negative values color')

    ###### Add DX features
    add_dx_features_group = add_dx.add_argument_group('add DX features group')
    add_dx_features_group.add_argument('-f', '--dx-features', required=True, action="append", default=[], help='Add dx features. If set together with -s/--dx-data-file or -t/--dx-data-track, features will be used as dynamix locations for the indicated signal file/track. Features can themselves be dynamically shown, see -i/--index-features')
    #EXTRA CONFIG  [ '--config', '{"displayMode" : "compact", "maxHeight" : 50}', '--clientConfig', '{"strandArrow" : false}']
    add_dx_features_group.add_argument('--display-mode', action="store", default="collapsed", type=str, help='Display mode of the feature track e.g. collapsed')
    add_dx_features_group.add_argument('--feat-color', action="store", default="blue", type=str, help='Color of the indexed features tracks')
    add_dx_features_group.add_argument('--max-height', action="store", default=50, type=int, help='height of the features tracks')
    add_dx_features_group.add_argument('--strand-arrow', action="store_true", help='set flag if strand arraw are needed')
    add_dx_features_group.add_argument('-rindex', '--replace-index', required=False, action="store_true",
                           default=False,  help="Force the re-indexing of known data, when an index with the same name already is found. Default (flag not set) to False")

    add_dx.add_argument('-d', '--defer-indexing', default=False, action='store_true', help="Prevent (re-)indexing the dynamix location track. It is necessary to index the track after adding new dynamix locations. However, if you want to add more than one track to Dynamix, you can defer the indexing step until last track to add. Default to False.")
    #
    add_dx.add_argument('--index-features', required=False, action='store_true', help="If -i flag is set, features indicated with -f/--dx-features will themselves be indexed as dynamic features. Default to False.")

    # ### RM DX TRACK
    # rm_dx_tck = dx_subparsers.add_parser('rm-dx-track', help='Remove a track from a Dynamix group.')
    # rm_dx_tck.add_argument('-g', '--dx-group', required=True, action='store', type=str,
    #                                          default=None, metavar="DYNAMIX_GROUP", help="Considered Dynamix group")
    # rm_dx_tck.add_argument('-t', '--dx-track', action="store", default="", type=str, help='dx traxck to remove')

    # ### RM DX GROUP
    # rm_dx_grp = dx_subparsers.add_parser('rm-dx-group', help='Remove a group from Dynamix configuration.')
    # rm_dx_grp.add_argument('-g', '--dx-group', required=True, action='store', type=str,
    #                                           default=None, metavar="DYNAMIX_GROUP", help="Considered Dynamix group")
    return dx_parser



if __name__ == '__main__':
    global __REINDEX
    global __REPLACE
    print "### Executing Dynamix ..."
    dx_args_parser = init_parser()
    dx_args = dx_args_parser.parse_args()
    print dx_args
    __REINDEX = dx_args.replace_index
    __REPLACE = dx_args.replace_config
    # Gather root path information
    jbrowse_directory = normalise_path(__DEFAULT_JBROWSE_DIR)
    jbrowse_conf = os.path.join(jbrowse_directory, "jbrowse.conf")
    print "### JBrowse root: {0}".format(jbrowse_directory)
    print "### JBrowse configuration: {0}".format(jbrowse_conf)
    #
    if jbrowse_directory and is_valid_jbrowse_directory(jbrowse_directory):
        try:
            print "### Reading JBrowse configuration...".format(os.path.basename(jbrowse_conf))
            jbrowse_config_content = read_jbrowse_configuration_file(jbrowse_conf)
            jbrowse_config_content_dic = jbrowse_conf_parser.get_jbrowse_conf_dictionary(jbrowse_config_content)
        except (SyntaxError, KeyError) as read_jbrowse_conf:
            print "### [WARN] {0}".format(read_jbrowse_conf)
            sys.exit("### Could not read jbrowse configuration (.conf).")
        else:
            dynamix_dir = os.path.join(jbrowse_directory, 'data/'+str(__PLUGIN_NAME.lower()))
            #
            default_location_tracks_dir = os.path.join(dynamix_dir, '/data/locationTracks/')
            # Gather I/O tracklists information
            print "### Dynamix group: {0}".format(dx_args.dx_group)
            default_group_dir = os.path.join(dynamix_dir, 'data', str(dx_args.dx_group))
            tracklist = os.path.join(default_group_dir, 'trackList.json')
            if dx_args.tracklist:
                tracklist = normalise_path(tracklist)
            print "### Dynamix trackList.json: {0}".format(tracklist)
            # Define Jbrowse indexing dir
            dynamix_group_dir = os.path.dirname(tracklist)

            # make dynamix dirs
            try:
                make_directory(dynamix_dir)
                make_directory(dynamix_group_dir)
            except OSError as make_struct_exc:
                print "###", repr(make_struct_exc), make_struct_exc
                sys.exit("Could not build dynamix path structure within jbrowse directories")

            ###
            data_root_keyword = jbrowse_config_content_dic.get('GENERAL', {}).get('dataRoot', [__DATA_ROOT_KEYWORD])[0]
            included_paths = jbrowse_config_content_dic.get('GENERAL', {}).get('include', [])
            included_confs = [os.path.join(jbrowse_directory, safe_dataroot_path(include_path, data_root_keyword))
                                         if not os.path.isabs(include_path)
                                         else safe_dataroot_path(include_path, data_root_keyword)
                                         for include_path in included_paths]

            main_dynamix_conf_path = os.path.join(dynamix_dir, "dynamix_conf.json")

            # read dynamix configuration
            try:
                main_dynamix_conf = read_json_configuration(main_dynamix_conf_path)
            except (OSError, IOError, ValueError) as read_mainconf_exc:
                main_dynamix_conf = {}

            main_dynamix_conf = set_default_empty_main_dynamix_configuration(main_dynamix_conf)
            main_dynamix_conf, dynamix_group_data_bed_file, dynamix_group_data_bed_trackname = set_default_dynamix_group_configuration(main_dynamix_conf, dx_args.dx_group, included_confs, tracklist, dynamix_dir)
            print "### Group location file: {0}".format(dynamix_group_data_bed_file)

            ## Reset tracklist priority tags
            # try:
                # reset_dynamix_tracklist_priority_tags(tracklist)
            # except (OSError, IOError, ValueError) as reset_tracklist_exc:
                # pass # does not matter.

            ## Read the bed file for features locations
            try:
                dynamix_bed_content_dic = jbrowse_bed_parser.read_bed_file_to_dictionary(dynamix_group_data_bed_file)
            except (OSError, IOError) as read_bed_err:
                print "###", repr(read_bed_err), read_bed_err
                dynamix_bed_content_dic = {}
            #     try:
            #         open(dynamix_group_data_bed_file, 'w').close()
            #     except (OSError, IOError) as create_bed_err:
            #         print "### Could not create empty location group file.".format(os.path.dirname(dynamix_group_data_bed_file))
            #         print "### {0}".format(create_empty_conf)
            #         sys.exit("### Exiting script.")
            # sys.exit()
            # dynamix_data_folder, dynamix_data_file_ext = os.path.split(dynamix_group_data_bed_file)
            # dynamix_trackname, _tmp = os.path.splitext(dynamix_data_file_ext)

            # extract key:val of interest in track configurations
            # tracklist_tracklabel_dxid_ptag_tuplist = get_tracklist_tracklabel_dxid_ptag_tuplist(tracks_configurations)
            # tracklist_tracklabels = [tlabel for tlabel, _, _ in tracklist_tracklabel_dxid_ptag_tuplist if tlabel]

            added_ids = set()
            if dx_args.command == "add-dx-data":
                added_signal_id = None
                if dx_args.signal or dx_args.track:
                    if dx_args.signal:
                        # A signal data file has been given by user.
                        # We create the track configuration snippet if it does not exist and add it to trackList.json
                        print "\n### [Add Dynamix dataset] [...]"
                        ##### Add datasets to JBrowse
                        ##### /!\ dynamix locations for these datasets must be added separately
                        ##### with the --dx-locations parameter)
                        try:
                            added_signal_id = add_dynamix_dataset(jbrowse_directory, dynamix_group_dir, tracklist, dx_args.signal, dx_args.display_name, dx_args.id, dx_args.xyplot, dx_args.xyplot_height, dx_args.xyplot_poscolor, dx_args.xyplot_negcolor, dx_args.heatmap, dx_args.heatmap_height, dx_args.heatmap_poscolor, dx_args.heatmap_negcolor, dx_args.min, dx_args.max, dx_args.autoscaling)
                        except (OSError, RuntimeError) as add_signal_error:
                            print "###", repr(add_signal_error), add_signal_error
                            added_signal_id = None
                        else:
                            if added_signal_id: added_ids.add(dx_args.id)
                        print "### [Add Dynamix dataset] [Done]"
                print ""
                if dx_args.dx_features:
                    print "### [Add Dynamix features] [...]"
                    ##### Add dynamix features that will be dynamically shown.
                    ##### Features are both a dataset and its dynamix locations
                    print "### Given feature file(s) [-f/--dx-features]: x{0}".format(len(dx_args.dx_features))
                    for feat in dx_args.dx_features:
                        print "\t- {0}".format(feat)
                    for dx_features_file_path in dx_args.dx_features:
                        added_feature_id = None
                        #if --index-features
                        # if the feature track label is the same then we already added that file! or a file with the same name
                        # if track with the same features track id > same ID has been given, so it is probably for the same group of tracks.
                        # but we need to increment ptag for the
                        added_feature_id = None
                        if dx_args.index_features:
                            try:
                                # feature_file_path, tracklist, jbrowse_directory, feature_dx_id, dynamix_group_dir
                                 added_feature_id = add_dynamix_feature(dx_features_file_path, tracklist, jbrowse_directory, dx_args.id, dynamix_group_dir)
                            except (OSError, RuntimeError) as index_oserr:
                                print "###", repr(index_oserr), index_oserr
                                added_feature_id = None
                            else:
                                if added_feature_id:
                                    added_ids.add(added_feature_id)
                            if added_signal_id or added_feature_id:
                                # we have the indexed track dynamix id. we now add the region to dynamix bed.
                                print "### Adding known locations from {0} [...]".format(os.path.basename(dx_features_file_path))
                                dynamix_bed_content_dic = add_dynamix_locations(dynamix_bed_content_dic, dx_features_file_path,
                                                                                                                    added_signal_id, added_feature_id)
                                print "### Adding known locations from {0} [Done]".format(os.path.basename(dx_features_file_path))
                        else:
                            print "### Nothing to do"
                    main_dynamix_conf = update_group_track_ids(main_dynamix_conf, dx_args.dx_group, added_ids)
            print ""
            # Write the new config
            jbrowse_json_handler.write_json_file(main_dynamix_conf_path, main_dynamix_conf)
            # Overwrite the group dynamix location file with the new dictionary
            jbrowse_bed_parser.overwrite_bed_file_with_dic(dynamix_group_data_bed_file, dynamix_bed_content_dic)
            print ""
            print "### [Indexing group track] {0} [...]".format(dx_args.dx_group),
            if not dx_args.defer_indexing:
                # Index the new bed file
                print "\n### Indexing can take time...(set the -d parameter for deferring that step)."
                jbrowse_index_utils.call_jbrowse_bed_index_script(os.path.join(jbrowse_directory, 'bin', "flatfile-to-json.pl"),
                                                                                                   dynamix_group_data_bed_file,
                                                                                                   dynamix_group_data_bed_trackname,
                                                                                                   dynamix_group_dir,
                                                                                                   [ '--config', '{"displayMode" : "collapsed"}', '--clientConfig', '{"strandArrow" : false }'])
                print "### [Indexing group track] {0} [Done]".format(dx_args.dx_group)
                # print "### Generate names [...]"
                # generatename_script = os.path.join(jbrowse_directory, "bin","generate-names.pl")
                # try:
                #     dxu.call_jbrowse_script(generatename_script, ['--out', jbrowse_directory])
                # except (OSError, RuntimeError) as gen_names_err:
                #     print "### Generate names [FAILED]", repr(gen_names_err), gen_names_err
                # else:
                #     print "### Generate names [Done]"
            else:
                print "- Deferred"

    else:
        sys.exit("# JBrowse directory {0} does not exist or is not valid.\nExiting.".format(jbrowse_directory))
